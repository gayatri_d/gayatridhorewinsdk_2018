#include"ClassFactoryDllServerWithRegFile.h"

/*Global varible declaration*/
long glNumberOfActiveComponents = 0;
long glNumberOfServerLock = 0;

/*Declaration of class*/
class CSumSubtract :public ISum, ISubtract
{
private:
	long m_cRef;
public:
	/*Constructor of class*/
	CSumSubtract();
	/*Destructor of class*/
	~CSumSubtract();
	/*IUnkown specific method declarations (inherited)*/
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release();

	/*ISum specific method declarations (inherited)*/
	HRESULT __stdcall SumOfTwoIntegers(int, int, int*);
	/*ISubtract specific method declarations (inherited)*/
	HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*);
};

class CSumSubtractClassFactory :public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method of class*/
	CSumSubtractClassFactory();
	/*Destructor method of class*/
	~CSumSubtractClassFactory();
	/*IUnkown specific method declarations (inherited)*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*IClassFactory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void **);
	HRESULT __stdcall LockServer(BOOL);
};

/*Dll Main*/

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	//code
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
		
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Constructor method of class CSumSubtract*/
CSumSubtract::CSumSubtract(void)
{
	//code
	m_cRef = 1; //hardcoded intialization to anticipate possible failure of QueryInterface()
	InterlockedIncrement(&glNumberOfActiveComponents); //Increment global counter
}

/*Destructor method of class CSumSubtract*/
CSumSubtract::~CSumSubtract(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents);
}

/*Impelementattion of CSumSubtract's IUnkown's Method*/
/*Parameters:
REFIID -> Reference to actual Interface i.e Sum/Subtract
ppv -> void pointer*/
HRESULT CSumSubtract::QueryInterface(REFIID riid, void **ppv)
{
	//code
	if (riid == IID_IUnknown)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISum)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISubtract)
		*ppv = static_cast<ISubtract *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubtract::AddRef(void)
{
	//code

	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubtract::Release(void)
{
	//code
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of ISum's Method*/
HRESULT CSumSubtract::SumOfTwoIntegers(int num1, int num2, int *pSum)
{
	*pSum = num1 + num2;
	return(S_OK);
}

/*Implementation of ISubtract's Method*/
HRESULT CSumSubtract::SubtractionOfTwoIntegers(int num1, int num2, int *pSubtract)
{
	*pSubtract = num1 - num2;
	return(S_OK);
}

/*Implementation of CSumSubtractClassFactory's constructor Method*/
CSumSubtractClassFactory::CSumSubtractClassFactory(void)
{
	m_cRef = 1; //hardcoded initialization to anticipate possible failure of QueryInterface()
}

/*Implementation of CSumSubtractClassFactory's destructor method*/
CSumSubtractClassFactory::~CSumSubtractClassFactory(void)
{
	//code
}

/*Implementation of CSumSubtractClassFactory's IClassFactory's IUnknown's Method*/
/*Parameters:
REFIID -> IID of CSumSubtractClassFactory */
HRESULT CSumSubtractClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	//code
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

/*Implementation of IUnknown's AddRef() Method*/
ULONG CSumSubtractClassFactory::AddRef(void)
{
	//code
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

/*Implementation of IUnknown's Release() method*/
ULONG CSumSubtractClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(S_OK);
}

/*Implementation of CSumSubtractClassFactory's IClassFactory's Method*/
HRESULT CSumSubtractClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	//variable declarations
	CSumSubtract *pCSumSubtract = NULL;
	HRESULT hr;

	//code
	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	//create the instance of component i.e of CSumSubtract class
	pCSumSubtract = new CSumSubtract;
	
	if (pCSumSubtract == NULL)
		return(E_OUTOFMEMORY);
	//get the requested interface 
	hr = pCSumSubtract->QueryInterface(riid, ppv);
	pCSumSubtract->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT CSumSubtractClassFactory::LockServer(BOOL fLock)
{
	//code
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLock);
	else
		InterlockedDecrement(&glNumberOfServerLock);
	
	return(S_OK);
}

/*Implementation of Exported functions from this dll*/
HRESULT __stdcall DllGetClassObject(REFCLSID  rclsid, REFIID riid, void **ppv)
{
	//variable declration
	CSumSubtractClassFactory *pCSumSubtractClassFactory = NULL;
	HRESULT hr;
	//code
	if (rclsid != CLSID_SumSubtract)
		return(CLASS_E_CLASSNOTAVAILABLE);
	//Create class factory
	pCSumSubtractClassFactory = new CSumSubtractClassFactory;
	if (pCSumSubtractClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCSumSubtractClassFactory->QueryInterface(riid, ppv);
	pCSumSubtractClassFactory->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	//code
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLock == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
