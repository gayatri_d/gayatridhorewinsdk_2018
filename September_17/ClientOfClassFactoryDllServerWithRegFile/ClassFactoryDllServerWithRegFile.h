#include<Windows.h>

/*Inheriting IUnknown Publicaly*/
class ISum:public IUnknown
{
	/*This is pure virtual class*/
	/*Return value of customized interface should be HRESULT (COM compulsion)*/
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
};

class ISubtract :public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

/*CLSID of SumSubtract component {FEF4962A-6128-44C9-BCF1-5A114AF3C392}*/
const CLSID CLSID_SumSubtract = { 0xfef4962a, 0x6128, 0x44c9, 0xbc, 0xf1, 0x5a, 0x11, 0x4a, 0xf3, 0xc3, 0x92 };

/*IID of ISum is {1223E481-86B1-4D90-A3A6-D892712F57A6}*/
const IID IID_ISum = { 0x1223e481, 0x86b1, 0x4d90, 0xa3, 0xa6, 0xd8, 0x92, 0x71, 0x2f, 0x57, 0xa6 };

/*IID of ISubtract is {304D57D1-F3A3-44BD-A851-683F692635CA}*/
const IID IID_ISubtract = { 0x304d57d1, 0xf3a3, 0x44bd, 0xa8, 0x51, 0x68, 0x3f, 0x69, 0x26, 0x35, 0xca };
