/*32-Bit Automation DLL server with reg file, giving functionality of Sum of two integers and subtraction of two integers*/

/*Include headers*/
#include"AutomationDllServerWithRegFile64Bit.h"
#include <stdio.h> //for swprintf_s()

/*CoClass Declaration*/
class CMyMath : public IMyMath
{
private:
	long m_cRef;
	ITypeInfo *m_pITypeInfo = NULL;

public:
	/*Constructor method*/
	CMyMath(void);
	/*Destructor method*/
	~CMyMath(void);

	/*IUnkown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*IDipatch specific methods*/
	HRESULT __stdcall GetTypeInfoCount(UINT*);
	HRESULT __stdcall GetTypeInfo(UINT, LCID, ITypeInfo**);
	HRESULT __stdcall GetIDsOfNames(REFIID, LPOLESTR*, UINT, LCID, DISPID*);
	HRESULT __stdcall Invoke(DISPID, REFIID, LCID, WORD, DISPPARAMS*, VARIANT*, EXCEPINFO*, UINT*);

	/*IMyMath specific methods*/
	HRESULT __stdcall SumOfTwoIntegers(int, int, int*);
	HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*);

	/*Custom Method*/
	HRESULT InitInstance(void);
};

/*Class factory declaration*/
class CMyMathClassFactory : public IClassFactory
{
private:
	long m_cRef;

public:
	/*Constructor Method*/
	CMyMathClassFactory(void);
	/*Destructor method*/
	~CMyMathClassFactory(void);

	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*IClassFactory Specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*Global Variables*/
long glNumberOfActiveComponents = 0;
long glNumberOfServerLocks = 0;

// {AFF99F80-592E-42B6-983A-3EDDB227F5D3}
const GUID LIBID_AutomationDllServer = { 0xaff99f80, 0x592e, 0x42b6, 0x98, 0x3a, 0x3e, 0xdd, 0xb2, 0x27, 0xf5, 0xd3 };

/*Dll Main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Implementation of CMyMath's constructor method*/
CMyMath::CMyMath(void)
{
	/*Hardcoded initialize to anticipate possible failure of QueryInterface*/
	m_cRef = 1;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

/*Implementation of CMyMath's Destructor method*/
CMyMath::~CMyMath()
{
	InterlockedDecrement(&glNumberOfActiveComponents);
}

/*Implementation of CMyMath's IUnknown's Methods*/
HRESULT CMyMath::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IMyMath *>(this);
	else if (riid == IID_IDispatch)
		*ppv = static_cast<IMyMath *>(this);
	else if (riid == IID_IMyMath)
		*ppv = static_cast<IMyMath *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}


ULONG CMyMath::AddRef()
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMyMath::Release()
{
	InterlockedDecrement(&m_cRef);

	if (m_cRef == 0)
	{
		m_pITypeInfo->Release();
		m_pITypeInfo = NULL;
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of IDispatch's methods*/
HRESULT CMyMath::GetTypeInfoCount(UINT *pCountTypeInfo)
{
	/*As we have type library her=nce 1 else 0*/
	*pCountTypeInfo = 1;
	return(S_OK);
}

HRESULT CMyMath::GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo **ppITypeInfo)
{
	*ppITypeInfo = NULL;

	if (iTypeInfo != 0)
		return(DISP_E_BADINDEX);

	m_pITypeInfo->AddRef();

	*ppITypeInfo = m_pITypeInfo;

	return(S_OK);
}

HRESULT CMyMath::GetIDsOfNames(REFIID riid, LPOLESTR *rgszNames, UINT cNames, LCID lcid, DISPID *rgDispId)
{
	return(DispGetIDsOfNames(m_pITypeInfo, rgszNames, cNames, rgDispId));
}

HRESULT CMyMath::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS *pDispParams, VARIANT *pVarResult,
	EXCEPINFO *pExcepInfo, UINT *puArgErr)
{
	/*Variable declarations*/
	HRESULT hr;

	hr = DispInvoke(this, m_pITypeInfo, dispIdMember, wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);
	return(hr);
}

/*Implementation of IMyMath's methods*/
HRESULT CMyMath::SumOfTwoIntegers(int num1, int num2, int *pSum)
{
	*pSum = num1 + num2;
	return(S_OK);
}

HRESULT CMyMath::SubtractionOfTwoIntegers(int num1, int num2, int *pSub)
{
	*pSub = num1 - num2;
	return(S_OK);
}

/*Custom Method*/
HRESULT CMyMath::InitInstance(void)
{
	/*Function declaration*/
	void ComErrorDescriptionString(HWND, HRESULT);

	/*Variables declarations*/
	HRESULT hr;
	ITypeLib *pITypeLib = NULL;

	/*Code*/
	if (m_pITypeInfo == NULL)
	{
		hr = LoadRegTypeLib(LIBID_AutomationDllServer,
			1, 0, 0x00, &pITypeLib);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			return(hr);
		}

		hr = pITypeLib->GetTypeInfoOfGuid(IID_IMyMath, &m_pITypeInfo);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			pITypeLib->Release();
			return(hr);
		}
		pITypeLib->Release();
	}
	return(S_OK);
}

/*Implementation of CMyMathClassFactory's Constructor method*/
CMyMathClassFactory::CMyMathClassFactory(void)
{
	m_cRef = 1;
}

CMyMathClassFactory::~CMyMathClassFactory(void)
{
	//code
}

/*Implementation of CMyMathClassFactory's IClassFactory's IUnknown's method*/
HRESULT CMyMathClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMyMathClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMyMathClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(S_OK);
	}
	return(m_cRef);
}

/*Implementation of CMyMathClassFactory's IClassFactory's methods*/
HRESULT CMyMathClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	/*Variable declrations*/
	CMyMath *pCMyMath = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);

	/*Create instance of component i.e of CMyMath class*/
	pCMyMath = new CMyMath;
	if (pCMyMath == NULL)
		return(E_OUTOFMEMORY);

	/*Call Automation releated init method*/
	pCMyMath->InitInstance();

	/*Get the requested interface*/
	hr = pCMyMath->QueryInterface(riid, ppv);

	pCMyMath->Release();

	return(hr);
}

HRESULT CMyMathClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);

	return(S_OK);
}

/*Implementation of exported functions from this Dll*/
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	/*Variable declarations*/
	CMyMathClassFactory *pCMyMathClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_MyMath)
		return(CLASS_E_CLASSNOTAVAILABLE);

	/*Create class factory*/
	pCMyMathClassFactory = new CMyMathClassFactory;

	if (pCMyMathClassFactory == NULL)
		return(E_OUTOFMEMORY);

	hr = pCMyMathClassFactory->QueryInterface(riid, ppv);
	pCMyMathClassFactory->Release();
	return(hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	/*Variable declration*/
	TCHAR *szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
		hr = HRESULT_CODE(hr);

	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&szErrorMessage, 0, NULL) != 0)
	{
		wsprintf(str, TEXT("%#x : %s"), hr, szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
	{
		wsprintf(str, TEXT("[Colud not find a description for error # %#x.]\n"), hr);
	}
	MessageBox(hwnd, str, TEXT("COM Error"), MB_OK);
}
