#pragma once

/*Include header*/
#include<windows.h>

class IMyMath : public IDispatch
{
public:
	/*Pure virtual*/
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

/*CLSID of IMyMath CoClass {45CEBD13-67C0-4B2E-A69A-0466F2C478F1}*/
const CLSID CLSID_MyMath = { 0x45cebd13, 0x67c0, 0x4b2e, 0xa6, 0x9a, 0x4, 0x66, 0xf2, 0xc4, 0x78, 0xf1 };

/*IID of IMyMath interface {72F06E95-0CFC-4E52-9D5F-292B70760D13}*/
const IID IID_IMyMath = { 0x72f06e95, 0xcfc, 0x4e52, 0x9d, 0x5f, 0x29, 0x2b, 0x70, 0x76, 0xd, 0x13 };
