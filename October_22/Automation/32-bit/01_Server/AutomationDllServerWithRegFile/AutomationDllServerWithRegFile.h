#pragma once

/*Include header*/
#include<windows.h>

class IMyMath : public IDispatch
{
public:
	/*Pure virtual*/
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

/*CLSID of IMyMath CoClass {E5908686-08C2-4804-8E18-CBA06B817058} */
const CLSID CLSID_MyMath = { 0xe5908686, 0x8c2, 0x4804, 0x8e, 0x18, 0xcb, 0xa0, 0x6b, 0x81, 0x70, 0x58 };

/*IID of IMyMath interface {96910B08-F207-432C-B3B5-FD699845771A} */
const IID IID_IMyMath = { 0x96910b08, 0xf207, 0x432c, 0xb3, 0xb5, 0xfd, 0x69, 0x98, 0x45, 0x77, 0x1a };
