#include"IDispatchAutomationClient.h"

/*Global function declaration*/
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*Global variable declration*/
IMyMath *pIMyMath = NULL;

/*WinMain*/
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpcmdLine, int iCmdShow)
{
	//Variable declrations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR AppName[] = TEXT("ComClient");

	/*wndclass initialization*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = AppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create window*/
	hwnd = CreateWindow(AppName,
		TEXT("Client of COM Automation"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	/*Message loop*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

/*Window procedure*/
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	/*Function declaration*/
	void ComErrorDescriptionString(HWND, HRESULT);

	/*Variable declarations*/
	IDispatch *pIDispatch = NULL;
	HRESULT hr;
	DISPID dispid;
	OLECHAR *szFunctionName1 = L"SumOfTwoIntegers";
	OLECHAR *szFunctionName2 = L"SubtractionOfTwoIntegers";
	VARIANT vArg[2], vRet;
	DISPPARAMS param = { vArg, 0, 2, NULL};
	int iNum1, iNum2;
	TCHAR str[255];

	switch (iMsg)
	{
	case WM_CREATE:
		/*Initialize COM library*/
		hr = CoInitialize(NULL);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("COM Library can not be initialized"), TEXT("COM Error"), MB_OK);
			DestroyWindow(hwnd);
			exit(0);
		}

		/*Get ISum Interface*/
		hr = CoCreateInstance(CLSID_MyMath, NULL, CLSCTX_INPROC_SERVER, IID_IDispatch, (void **)&pIDispatch);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Component Can not be Created"), TEXT("COM Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			DestroyWindow(hwnd);
			exit(0);
		}

		/*Common code for both IMyMath->SumOfTwoIntegers() and IMyMath->SubtractionOfTwoIntegers() */
		iNum1 = 146;
		iNum2 = 76;

		VariantInit(vArg);
		vArg[0].vt = VT_INT;
		vArg[0].intVal = iNum2;
		vArg[1].vt = VT_INT;
		vArg[1].intVal = iNum1;
		param.cArgs = 2;
		param.cNamedArgs = 0;
		param.rgdispidNamedArgs = NULL;
		param.rgvarg = vArg;

		VariantInit(&vRet);

		/*Code for IMyMath->SumOfTwoIntegers() */
		hr = pIDispatch->GetIDsOfNames(IID_NULL, &szFunctionName1, 1, GetUserDefaultLCID(), &dispid);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not get ID for SumOfTwoIntegers()"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIDispatch->Release();

			DestroyWindow(hwnd);
		}

		hr = pIDispatch->Invoke(dispid,
			IID_NULL,
			GetUserDefaultLCID(),
			DISPATCH_METHOD,
			&param,
			&vRet,
			NULL,
			NULL);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not Invoke Function"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIDispatch->Release();
			
			DestroyWindow(hwnd);
		}
		else
		{
			wsprintf(str, TEXT("Sum of %d and %d is %d"), iNum1, iNum2, vRet.lVal);
			MessageBox(hwnd, str, TEXT("Sum Result"), MB_OK);
		}

		/*Code for IMyMath->SubtractionOfTwoIntegers() */
		hr = pIDispatch->GetIDsOfNames(IID_NULL,
			&szFunctionName2, 
			1, 
			GetUserDefaultLCID(), 
			&dispid);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(NULL, TEXT("Can not get ID for SubtractionOfTwoIntegers()"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIDispatch->Release();

			DestroyWindow(hwnd);
		}
		vRet.lVal = 0;
		/*Invoke for IMyMath->SubtractionOfTwoIntegers()*/
		hr = pIDispatch->Invoke(dispid,
			IID_NULL,
			GetUserDefaultLCID(),
			DISPATCH_METHOD,
			&param,
			&vRet,
			NULL,
			NULL);
		
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(NULL, TEXT("Can not invoke function"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			pIDispatch->Release();

			DestroyWindow(hwnd);
		}
		else
		{
			wsprintf(str, TEXT("Subtraction of %d and %d is %d"), iNum1, iNum2, vRet.lVal);
			MessageBox(hwnd, str, TEXT("Subtraction result"), MB_OK);
		}

		/*Clean up*/
		VariantClear(vArg);
		VariantClear(&vRet);
		pIDispatch->Release();
		pIDispatch = NULL;
		DestroyWindow(hwnd);
		break;

		case WM_DESTROY:
			CoUninitialize();
			PostQuitMessage(0);
			break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	/*Variable declration*/
	TCHAR *szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
		hr = HRESULT_CODE(hr);

	if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&szErrorMessage, 0, NULL) != 0)
	{
		wsprintf(str, TEXT("%s"), szErrorMessage);
		LocalFree(szErrorMessage);
	}
	else
		wsprintf(str, TEXT("[Could not find the description for error # %#x.]\n"), hr);

	MessageBox(hwnd, str, TEXT("Error"), MB_OK);
}
