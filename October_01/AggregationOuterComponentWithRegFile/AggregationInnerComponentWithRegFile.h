#include<windows.h>

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of MultiplicationDivison component*/
// {CD0FECEA-2837-4ECB-B6A1-464ADF6DC22B}
const CLSID CLSID_MultiplicationDivison = { 0xcd0fecea, 0x2837, 0x4ecb, 0xb6, 0xa1, 0x46, 0x4a, 0xdf, 0x6d, 0xc2, 0x2b };

/*IID of IMultiplication interface*/
// {6308440B-18B7-49B1-8DB0-7F4D9B53FCCD}
const IID IID_IMultiplication = { 0x6308440b, 0x18b7, 0x49b1, 0x8d, 0xb0, 0x7f, 0x4d, 0x9b, 0x53, 0xfc, 0xcd };

/*IID of IDivison interface*/
// {65EFEA87-2286-4600-A6C0-D44C2EE68813}
const IID IID_IDivison = { 0x65efea87, 0x2286, 0x4600, 0xa6, 0xc0, 0xd4, 0x4c, 0x2e, 0xe6, 0x88, 0x13 };
