#include<windows.h>

class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
};

class ISubtract : public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

/*CLSID of SumSubtract component {2C3E2852-6543-4344-9123-BDDAC7A9A032}*/
const CLSID CLSID_SumSubtract = { 0x2c3e2852, 0x6543, 0x4344, 0x91, 0x23, 0xbd, 0xda, 0xc7, 0xa9, 0xa0, 0x32 };

/*IID of ISum interface {3FFFAE25-8566-46C8-8A00-E6853E50A5BA}*/
const IID IID_ISum = { 0x3fffae25, 0x8566, 0x46c8, 0x8a, 0x0, 0xe6, 0x85, 0x3e, 0x50, 0xa5, 0xba };

/*IID of  ISubtract interface {8856A11B-5C58-4D85-9FB4-51267D72BAD9}*/
const IID IID_ISubtract = { 0x8856a11b, 0x5c58, 0x4d85, 0x9f, 0xb4, 0x51, 0x26, 0x7d, 0x72, 0xba, 0xd9 };
