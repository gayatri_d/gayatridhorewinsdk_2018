#include<windows.h>

class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
};

class ISubtract : public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of SumSubtract component {2C3E2852-6543-4344-9123-BDDAC7A9A032}*/
const CLSID CLSID_SumSubtract = { 0x2c3e2852, 0x6543, 0x4344, 0x91, 0x23, 0xbd, 0xda, 0xc7, 0xa9, 0xa0, 0x32 };

/*IID of ISum interface {3FFFAE25-8566-46C8-8A00-E6853E50A5BA}*/
const IID IID_ISum = { 0x3fffae25, 0x8566, 0x46c8, 0x8a, 0x0, 0xe6, 0x85, 0x3e, 0x50, 0xa5, 0xba };

/*IID of  ISubtract interface {8856A11B-5C58-4D85-9FB4-51267D72BAD9}*/
const IID IID_ISubtract = { 0x8856a11b, 0x5c58, 0x4d85, 0x9f, 0xb4, 0x51, 0x26, 0x7d, 0x72, 0xba, 0xd9 };

/*IID of IMultiplication interface*/
// {6308440B-18B7-49B1-8DB0-7F4D9B53FCCD}
const IID IID_IMultiplication = { 0x6308440b, 0x18b7, 0x49b1, 0x8d, 0xb0, 0x7f, 0x4d, 0x9b, 0x53, 0xfc, 0xcd };

/*IID of IDivison interface*/
// {65EFEA87-2286-4600-A6C0-D44C2EE68813}
const IID IID_IDivison = { 0x65efea87, 0x2286, 0x4600, 0xa6, 0xc0, 0xd4, 0x4c, 0x2e, 0xe6, 0x88, 0x13 };
