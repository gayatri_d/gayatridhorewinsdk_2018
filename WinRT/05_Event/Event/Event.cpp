#include"pch.h"
#include"Event.h"

void MyCallBackMethod(Windows::UI::Xaml::ApplicationInitializationCallbackParams^ params)
{
	Event^ event = ref new Event();
}

int main(Array <String^ > ^ args)
{
	//MessageDialog^ msg = ref new MessageDialog("I am inside main");
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallBackMethod);
	Application::Start(callback);
	return(0);
}

void Event::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ agrs)
{
	MyPage^ page = ref new MyPage();

	Window::Current->Content = page;
	Window::Current->Activate();
}
