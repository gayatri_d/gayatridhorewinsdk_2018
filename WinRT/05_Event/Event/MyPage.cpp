#include"pch.h"
#include"MyPage.h"

MyPage::MyPage()
{
	//MessageDialog^ msg = ref new MessageDialog("I am inside MyPage Constructor");
	Window::Current->CoreWindow->KeyDown += ref new TypedEventHandler<CoreWindow^, KeyEventArgs^>(this, &MyPage::OnKeyDown);
	Grid^ grid = ref new Grid;
	textblock = ref new TextBlock;
	textblock->Text = "This is Event Handler";
	textblock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textblock->FontSize = 90;
	textblock->FontStyle = Windows::UI::Text::FontStyle::Normal;
	textblock->FontWeight = Windows::UI::Text::FontWeights::Bold;
	textblock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textblock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textblock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	grid->Children->Append(textblock);

	/*Button on window*/
	Button^ button = ref new Button;
	button->Content = "OK";
	button->Width = 400;
	button->Height = 100;
	button->BorderThickness = 12;
	button->BorderBrush = ref new SolidColorBrush(Windows::UI::Colors::Red);
	button->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	button->FontSize = 12;
	button->FontStyle = Windows::UI::Text::FontStyle::Oblique;
	button->FontWeight = Windows::UI::Text::FontWeights::Bold;
	button->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	button->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Bottom;
	button->Click += ref new RoutedEventHandler(this, &MyPage::OnButtonClick);
	grid->Children->Append(button);
	this->Content = grid;
}

void MyPage::OnKeyDown(CoreWindow^ Sender, KeyEventArgs^ args)
{
	if (args->VirtualKey == VirtualKey::Space)
	{
		textblock->Text = "Space key is pressed";
	}
}

void MyPage::OnButtonClick(Object^ Sender, RoutedEventArgs^ args)
{
	textblock->Text = "Mouse button is clicked";
}
