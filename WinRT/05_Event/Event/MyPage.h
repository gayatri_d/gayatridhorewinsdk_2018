#pragma once
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Text;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Core;
using namespace Windows::Foundation;
using namespace Windows::System;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Popups;

ref class MyPage sealed : Page
{
private:
	TextBlock^ textblock;
public:
	MyPage();
	void OnKeyDown(CoreWindow^ Sender, KeyEventArgs^ args);
	void OnButtonClick(Object^ Sender, RoutedEventArgs^ args);
};
