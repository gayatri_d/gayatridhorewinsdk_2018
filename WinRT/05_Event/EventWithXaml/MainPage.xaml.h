﻿//
// MainPage.xaml.h
// Declaration of the MainPage class.
//

#pragma once
using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Text;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Core;
using namespace Windows::Foundation;
using namespace Windows::System;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::UI::Popups;
#include "MainPage.g.h"

namespace hello
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public ref class MainPage sealed
	{
	public:
		MainPage();
		void OnKeyDown(CoreWindow^ Sender, KeyEventArgs^ args);
		void OnButtonClick(Object^ Sender, RoutedEventArgs^ args);
	};
}
