#pragma once
#include"pch.h"

using namespace Platform;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml;


ref class App sealed : public Windows::UI::Xaml::Application
{
public:
	virtual void OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ Arg)override;
};
