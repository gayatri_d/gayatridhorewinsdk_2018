#include"pch.h"
#include"hello.h"

void MyCallBackMethod(Windows::UI::Xaml::ApplicationInitializationCallbackParams^ params)
{
	App^ app = ref new App();
}

int main(Array <String^>^ args)
{
	ApplicationInitializationCallback^ callback = ref new ApplicationInitializationCallback(MyCallBackMethod);
	Application::Start(callback);
	return 0;
}
void App::OnLaunched(Windows::ApplicationModel::Activation::LaunchActivatedEventArgs^ Arg)
{
	TextBlock^ textBlock = ref new TextBlock;
	textBlock->Text = "Hello World!!!";
	textBlock->FontFamily = ref new Windows::UI::Xaml::Media::FontFamily("Segoe UI");
	textBlock->FontSize = 120;
	textBlock->FontStyle = Windows::UI::Text::FontStyle::Normal;
	//textBlock->FontWeight = Windows::UI::Text::FontWeight::Bold;
	textBlock->Foreground = ref new SolidColorBrush(Windows::UI::Colors::Gold);
	textBlock->HorizontalAlignment = Windows::UI::Xaml::HorizontalAlignment::Center;
	textBlock->VerticalAlignment = Windows::UI::Xaml::VerticalAlignment::Center;

	Window::Current->Content = textBlock;
	Window::Current->Activate();
}
