/*Assignment1.3: Message Handling - Use any 5 Messges and show message box per message use diffrent icons per message box*/
/*Student Name: Gayatri Dhore*/
/*Date: 30/07/2018 */

/*Include headers */
#include<Windows.h>

/*Function Prototype of Window Procedure */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*WinMain*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	/*Variable declaration */
	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("My App");
	MSG msg;

	/*Flushing the class structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	/*Initializing the class members*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create Window*/
	hwnd = CreateWindow(szAppName,
		TEXT("Message Handling"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	/*Update window - First time WM_PAINT message is sent */
	UpdateWindow(hwnd);

	/*Message loop*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM  wParam, LPARAM lParam)
{
	switch (iMsg)
	{
		/*Message 1*/
	case WM_CREATE:
		MessageBox(hwnd, TEXT("WELCOME!!!"), TEXT("Splash Screen"), MB_OK | MB_ICONINFORMATION | MB_TOPMOST);
		break;

		/*Message 2*/
	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("You clicked left button of mouse!"), TEXT("Message 2"),MB_OK | MB_ICONHAND | MB_TOPMOST);
		break;

		/*Message 3*/
	case WM_MOVE:
		MessageBox(hwnd, TEXT("Do you want move the window?"), TEXT("Message 3"), MB_YESNO | MB_ICONASTERISK | MB_TOPMOST);
		break;

		/*Message 4*/
	case WM_KEYDOWN:
		MessageBox(hwnd, TEXT("You Pressed the key!"), TEXT("Message 4"), MB_RETRYCANCEL | MB_ICONERROR | MB_TOPMOST);
		break;

		/*Message 5*/
	case WM_SIZE:
		MessageBox(hwnd, TEXT("You resize the window"), TEXT("Message 5"), MB_ABORTRETRYIGNORE | MB_ICONEXCLAMATION | MB_TOPMOST);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}