/*ASSIGNMENT 1_4
Create a window which will change the color based on key pressed
R - Red
G - Green
B - Blue
Y - Yellow
C - Cyan
M - Magenta
K - Black
W - White 
Studet Name: Gayatri Dhore*/

//Headers
#include<windows.h>

//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	/*Flush the wndclass structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	//code
	//Initialization of  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register  above class
	RegisterClassEx(&wndclass);

	//Create window
	hwnd = CreateWindow(szAppName,
		TEXT("MyApplication"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	/*hwnd: window handle which is to be shown,
	iCmdShow: Where to show the window default: SW_SHOWNORMAL
	Other values: SW_HIDE, SW_MAXIMIZE, SW_MINIMIZE*/
	ShowWindow(hwnd, iCmdShow);

	/*Paint this window: It is not required to call this function because it is internally called by VS,
	but as good programming practice you MUST call it*/
	UpdateWindow(hwnd);

	/*message loop*/
	/*Message Loop: Gives life to Window: Get -> Translate -> Dispatch msg
	GetMessage returns Boolean value (Typedef BOOL), returns FALSE only when msg is WM_QUIT*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	/*Variable Declarations*/
	static char key;
	HDC hdc;
	RECT rc;
	PAINTSTRUCT ps;
	/*Handle to brush */
	HBRUSH hBrush;

	//code
	switch (iMsg)
	{
	case WM_KEYDOWN:
		GetClientRect(hwnd, &rc);
		InvalidateRect(hwnd, NULL, TRUE);
		switch (wParam)
		{
			case 'R':
				key = 'R';
				break;
			case 'G':
				key = 'G';
				break;
			case 'B':
				key = 'B';
				break;
			case 'Y':
				key = 'Y';
				break;
			case 'C':
				key = 'C';
				break;
			case 'M':
				key = 'M';
				break;
			case 'W':
				key = 'W';
				break;
			case 'K':
				key = 'K';
				break;
			default:
				key = wParam;
				break;
			}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		switch (key)
		{
			case 'R':
				hBrush =(HBRUSH)CreateSolidBrush(RGB(255, 0, 0));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'G':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 255, 0));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;
			
			case 'B':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 0, 255));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'Y':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'M':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 0, 255));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'C':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 255, 255));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'W':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 255));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			case 'K':
				hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 0, 0));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;

			default:
				hBrush = (HBRUSH)CreateSolidBrush(RGB(128, 128, 128));
				SelectObject(hdc, hBrush);
				Rectangle(hdc, (int)rc.left, (int)rc.top, (int)rc.right, (int)rc.bottom);
				DeleteObject(hBrush);
				break;
		}
		EndPaint(hwnd, &ps);
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



