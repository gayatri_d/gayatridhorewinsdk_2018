/*Include headers*/
#include<Windows.h>

/*Global function declaration
Function Prototype */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	/*Variable declaration*/
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	/* TCHAR(In C) is UNICODE style 2 bytes per character. Range: 0-65536 */
	TCHAR szAppName[] = TEXT("My App");

	/*Flush the wndclass structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	/*Initialization of WNDCLASSEX*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	/*Register this callback function to class*/
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	/*Here we are not giving explicit Icon to our application,
	with IDI_APPLICATION parameter we are telling use default ICON
	use for other applications, IDI_APPLICATION is APPLICATION ICON's ID*/
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	/*Here we are ot giving explicit Cursor to application,
	with IDC_ARROW parameter we are telling use default cursor ARROW
	IDC_ARROW- ARROW CURSOR's ID */
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	/*Class name should be unique and it is hashed by OS by Appending/ Prepending some characters*/
	wndclass.lpszClassName = szAppName;
	/*No menu in our application*/
	wndclass.lpszMenuName = NULL;
	/*Application Icon when it is minimized. Here it is kept default*/
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create Winndow
	Arguments: szAppName: Name of class(hashed name)
	TEXT(): Window name on title bar,
	WS_OVERLAPPEDWINSDOW: Window style, it is overlapped on all other current windows of other applications,
	(WS_OVERLAPPEDWINSDOW is combination of 6 Window Style.)
	(WS_OVERLAPPEDwINDOW = WS_OVERLAPPED + WS_CAPTION + WS_THICKFRAME + WS_MINIMIZEBOX + WS_MAXIMIZEBOX + WS_ SYSMENU)
	CW_USEDEFAULT: It is x axis of Left Top of Window,
	CW_USEDEAFULT: It is y axis of Left Top of Window,
	CW_USEDEFAULT: It is width of window,
	CW_USEDEFAULT: It is height of window
	(All this values are sets to default the decision of where to show window now depends on OS),
	NULL: Handle of it's parent window,
	NULL: Menu Handle,
	hInstance : Current Intance's handle,
	NULL: Creation Parameters*/
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	/*hwnd: window handle which is to be shown,
	iCmdShow: Where to show the window default: SW_SHOWNORMAL
	Other values: SW_HIDE, SW_MAXIMIZE< SW_MINIMIZE*/
	ShowWindow(hwnd, SW_MAXIMIZE);

	/*Paint this window: It is not required to call this function because it is internally called by VS,
	but as good programming practice you MUST call it*/
	UpdateWindow(hwnd);

	/*Message Loop: Gives life to Window: Get -> Translate -> Dispatch msg
	GetMessage returns Boolean value (Typedef BOOL), returns FALSE only when msg is WM_QUIT*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	switch (iMsg)
	{
	case WM_DESTROY:
		/*WM_QUITE*/
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}
