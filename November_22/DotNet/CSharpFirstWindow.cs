using System;
using System.Drawing;
using System.Windows.Forms;

public class CSharpFirstWindow:Form
{
		public static void Main()
		{
				Application.Run(new CSharpFirstWindow());
		}
		public CSharpFirstWindow()
		{
				this.Width 	= 800;
				this.Height 	= 600;
				this.BackColor = Color.White;
				ResizeRedraw = true;
		}
}
