using System;
using System.Drawing;
using System.Windows.Forms;

public class CSharpEvent : Form
{
		public static void Main()
		{
			Application.Run(new CSharpEvent());
		}
		public CSharpEvent()
		{
			this.Width = 800;
			this.Height = 600;
			this.BackColor = Color.White;
			ResizeRedraw = true;
			this.KeyDown += new KeyEventHandler(MyKeyDown);
			this.MouseDown += new MouseEventHandler(MyMouseDown);			
		}
		
		/*Keyboard handler method*/
		void MyKeyDown(object Sender, KeyEventArgs e)
		{
			MessageBox.Show("Some key is pressed!");
		}
		
		/*Mouse handler method*/
		void MyMouseDown(object Sender, MouseEventArgs e)
		{
			MessageBox.Show("Some mouse button is pressed!");
		}
}
