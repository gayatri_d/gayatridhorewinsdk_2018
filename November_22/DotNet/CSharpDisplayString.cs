using System;
using System.Drawing;
using System.Windows.Forms;

public class CSharpDisplayString : Form
{
		public static void Main()
		{
				Application.Run(new CSharpDisplayString());
		}
		
		/*Constructor Method*/
		public CSharpDisplayString()
		{
			this.Width = 800;
			this.Height = 600;
			this.BackColor = Color.Black;
			ResizeRedraw = true;
		}
		
		/*Function to draw text on window*/
		protected override void OnPaint(PaintEventArgs pea)
		{
			Graphics grfx = pea.Graphics;
			StringFormat strfmt = new StringFormat();
			strfmt.Alignment = StringAlignment.Center;
			strfmt.LineAlignment = StringAlignment.Center;
			grfx.DrawString("Hello World!!!",
							Font,
							new SolidBrush(System.Drawing.Color.Green),
							ClientRectangle,
							strfmt);
		}
		
}
