using System;
using System.Drawing;
using System.Windows.Forms;

public class CSharpIcon : Form
{
		public static void Main()
		{
			Application.Run(new CSharpIcon());
		}
		
		public CSharpIcon()
		{
			Icon = new Icon("MyIcon.ico");
			this.Width = 800;
			this.Height = 600;
			ResizeRedraw = true;
			BackColor = Color.White;
		}
}
