//Headers
#include<windows.h>
#include<tchar.h>

//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Thread Proc functions
DWORD WINAPI MyThreadProcOne(LPVOID);
DWORD WINAPI MyThreadProcTwo(LPVOID);

//Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	//code
	//Initialization of  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register  above class
	RegisterClassEx(&wndclass);

	//Create window
	hwnd = CreateWindow(szAppName,
		TEXT("MyApplication"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	/*Thread handles are declared static so that they can be used across the messages */
	HANDLE static hThread1 = NULL;
	HANDLE static hThread2 = NULL;

	switch (iMsg)
	{
	case WM_CREATE:
		/*Creation of second thread*/
		hThread1 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadProcOne, (LPVOID)hwnd, 0, NULL);
		/*Error handling*/
		if (hThread1 == NULL)
		{
			MessageBox(hwnd, TEXT("Fail to create Thread 1"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			DestroyWindow(hwnd);
		}
		/*Creation of thirad thread */
		hThread2 = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadProcTwo, (LPVOID)hwnd, 0, NULL);
		if (hThread2 == NULL)
		{
			MessageBox(hwnd, TEXT("Fail to create Thread 2"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			DestroyWindow(hwnd);
		}
		break;

	case WM_LBUTTONDOWN:
		MessageBox(hwnd, TEXT("I am fourth thread"), TEXT("Message"), MB_OK | MB_ICONINFORMATION | MB_TOPMOST);
		break;

	case WM_DESTROY:
		CloseHandle(hThread1);
		CloseHandle(hThread2);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

DWORD WINAPI MyThreadProcOne(LPVOID params)
{
	HWND hwnd;
	HDC hdc;
	int i;
	TCHAR str[255];
	hwnd = (HWND)params;
	hdc = GetDC(hwnd);
	SetBkColor(hdc, RGB(0, 0, 0));
	SetTextColor(hdc, RGB(0, 255, 0));
	/*Displaying numbers in incrasing order*/
	for (i = 0; i <= 32767; i++)
	{
		wsprintf(str, TEXT("Thread 1 -> Incrementing the value = %d"), i);
		TextOut(hdc, 5, 5, str, (int)_tcslen(str));

	}
	ReleaseDC(hwnd, hdc);
	return(0);
}

DWORD WINAPI MyThreadProcTwo(LPVOID params)
{
	HWND hwnd;
	HDC hdc;
	int i;
	TCHAR str[255];
	hwnd = (HWND)params;
	hdc = GetDC(hwnd);
	SetBkColor(hdc, RGB(0, 0, 0));
	SetTextColor(hdc, RGB(0, 255, 0));
	/*Displaying numbers in decreasing order*/
	for (i = 32768; i >= 0; i--)
	{
		wsprintf(str, TEXT("Thread 2 -> Decrementing the value = %d"), i);
		TextOut(hdc, 5, 25, str, (int)_tcslen(str));

	}
	ReleaseDC(hwnd, hdc);
	return(0);
}
