/*Drawing Ganapati Bappa using multithreading */
#include<Windows.h>
#include "Ganpati_bappa.h"
#include<MMSystem.h>
#include<tchar.h>

/*Windo Procedure function */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*ThreadProc() Function */
DWORD WINAPI MyThreadOne(LPVOID);
DWORD WINAPI MyThreadTwo(LPVOID);
DWORD WINAPI MyThreadThree(LPVOID);
DWORD WINAPI MyThreadFour(LPVOID);
DWORD WINAPI MyAudioThread(LPVOID);

static HANDLE hThread_One;
static HANDLE hThread_Two;
static HANDLE hThread_Three;
static HANDLE hThread_Four;
static HANDLE hThreadAudio;

PAINTSTRUCT ps;
HDC hdc;
RECT rect;

void ThreadStart();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("Gajanan");

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;

	/*Register calss to OS*/
	RegisterClassEx(&wndclass);

	/*Create Window*/

	hwnd = CreateWindow(szAppName,
		TEXT("Gajanan"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	/*Message Loop*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{

	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_CREATE:
		/*First Thread*/
		hThread_One = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadOne, (LPVOID)hwnd, 0, NULL);
		if (hThread_One == NULL)
		{
			MessageBox(hwnd, TEXT("Thread One creation failed!"), TEXT("Error"), MB_OK);
		}
		SuspendThread(hThread_One);
		/*Second Thread*/
		hThread_Two = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadTwo, (LPVOID)hwnd, 0, NULL);
		if (hThread_Two == NULL)
		{
			MessageBox(hwnd, TEXT("Thread Two creation failed!"), TEXT("Error"), MB_OK);
		}
		SuspendThread(hThread_Two);
		/*Third Thread*/
		hThread_Three = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadThree, (LPVOID)hwnd, 0, NULL);
		if (hThread_Three == NULL)
		{
			MessageBox(hwnd, TEXT("Thread Three creation failed!"), TEXT("Error"), MB_OK);
		}
		SuspendThread(hThread_Three);
		/*Fourth Thread*/
		hThread_Four = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyThreadFour, (LPVOID)hwnd, 0, NULL);
		if (hThread_Four == NULL)
		{
			MessageBox(hwnd, TEXT("Thread Four creation failed!"), TEXT("Error"), MB_OK);
		}
		SuspendThread(hThread_Four);
		/*Audio Thread*/
		hThreadAudio = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)MyAudioThread, (LPVOID)hwnd, 0, NULL);
		if (hThreadAudio == NULL)
		{
			MessageBox(hwnd, TEXT("Audio Thread creation failed!"), TEXT("Error"), MB_OK);
		}
		SuspendThread(hThreadAudio);
		break;

	case WM_PAINT:
		//hdc = BeginPaint(hwnd, &ps);
		ThreadStart();
		//EndPaint(hwnd, &ps);
		break;

	case WM_DESTROY:
		CloseHandle(hThread_One);
		CloseHandle(hThread_Two);
		CloseHandle(hThread_Three);
		CloseHandle(hThread_Four);
		CloseHandle(hThreadAudio);
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}


DWORD WINAPI MyThreadOne(LPVOID Param)
{
	HWND hwnd;
	hwnd = (HWND)Param;
	HPEN hOrangePen;
	HDC hDC;
	int i;
	RECT rect;
	DWORD nPt = 4;
	TCHAR str[512] = TEXT("\x0917\x0923\x092A\x0924\x0940 \x092C\x093E\x092A\x093A\x092A\093E");
	hDC = GetDC(hwnd);
	hOrangePen = CreatePen(PS_SOLID, 2, RGB(255, 200, 0));
	SelectObject(hDC, hOrangePen);
	SelectObject(hDC, (HBRUSH)BLACK_BRUSH);
	GetClientRect(hwnd, &rect);
	//InvalidateRect(hwnd, &rect, FALSE);

	for (i = (3 * rect.right / 10); i < (2 * rect.right / 5); i++)
	{
		MoveToEx(hDC, (3 * rect.right / 10), (rect.bottom / 6), NULL);
		LineTo(hDC, i, (rect.bottom / 6));
		Sleep(10);
	}

	for (i = rect.bottom / 6; i < (rect.bottom / 3); i++)
	{
		MoveToEx(hDC, (2 * rect.right / 5), (rect.bottom / 6), NULL);
		LineTo(hDC, (2 * rect.right / 5), i);
		Sleep(10);
	}

	for (i = (2 * rect.right / 5); i >(3 * rect.right / 10); i--)
	{
		MoveToEx(hDC, (2 * rect.right / 5), (rect.bottom / 3), NULL);
		LineTo(hDC, i, (rect.bottom / 3));
		Sleep(10);
	}

	for (i = (rect.bottom / 3); i > rect.bottom / 6; i--)
	{
		MoveToEx(hDC, (3 * rect.right / 10), (rect.bottom / 3), NULL);
		LineTo(hDC, (3 * rect.right / 10), i);
		Sleep(10);
	}
	/*Points to draw first Arc*/
	POINT apt[4];
	apt[0].x = (3 * rect.right / 10);
	apt[0].y = rect.bottom / 6;
	apt[1].x = (7 * rect.right / 20);
	apt[1].y = rect.bottom / 12;
	apt[2].x = (9 * rect.right / 20);
	apt[2].y = rect.bottom / 12;
	apt[3].x = rect.right / 2;
	apt[3].y = rect.bottom / 6;
	/*Drawing an Arc*/
	for (i = 0; i < 4; i++)
		PolyBezier(hDC, apt, 4);

	/*Points to draw second Arc*/
	apt[0].x = (2 * rect.right / 5);
	apt[0].y = rect.bottom / 6;
	apt[1].x = (9 * rect.right / 20);
	apt[1].y = rect.bottom / 12;
	apt[2].x = (11 * rect.right / 20);
	apt[2].y = rect.bottom / 12;
	apt[3].x = (3 * rect.right / 5);
	apt[3].y = rect.bottom / 6;
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	MoveToEx(hDC, (2 * rect.right / 5), (rect.bottom / 3), NULL);
	LineTo(hDC, (rect.right / 2), rect.bottom / 2);
	MoveToEx(hDC, (rect.right / 2), (rect.bottom / 3), NULL);
	LineTo(hDC, (11 * rect.right / 20), (rect.bottom / 2));
	MoveToEx(hDC, (9 * rect.right / 20) - 10, (rect.bottom / 6), NULL);
	SelectObject(hDC, (HPEN)GetStockObject(WHITE_PEN));
	LineTo(hDC, (9 * rect.right / 20) + 10, (rect.bottom / 6));
	MoveToEx(hDC, (9 * rect.right / 20) - 10, (rect.bottom / 6) + 10, NULL);
	LineTo(hDC, (9 * rect.right / 20) + 10, (rect.bottom / 6) + 10);
	TextOut(hdc, 500, 0, str, _tcslen(str));
	ReleaseDC(hwnd, hDC);
	DeleteObject(hOrangePen);
	return(0);
}

DWORD WINAPI MyThreadTwo(LPVOID Param)
{
	HWND hwnd;
	hwnd = (HWND)Param;
	HPEN hOrangePen;
	HDC hDC;
	int i;
	RECT rect;
	DWORD nPt = 4;

	hDC = GetDC(hwnd);
	hOrangePen = CreatePen(PS_SOLID, 2, RGB(255, 200, 0));
	SelectObject(hDC, hOrangePen);
	SelectObject(hDC, (HBRUSH)BLACK_BRUSH);
	GetClientRect(hwnd, &rect);
	//InvalidateRect(hwnd, &rect, FALSE);

	for (i = (1 * rect.right / 2); i < (3 * rect.right / 5); i++)
	{
		MoveToEx(hDC, (1 * rect.right / 2), (rect.bottom / 6), NULL);
		LineTo(hDC, i, (rect.bottom / 6));
		Sleep(10);
	}

	for (i = rect.bottom / 6; i < (rect.bottom / 3); i++)
	{
		MoveToEx(hDC, (3 * rect.right / 5), (rect.bottom / 6), NULL);
		LineTo(hDC, (3 * rect.right / 5), i);
		Sleep(10);
	}

	for (i = (3 * rect.right / 5); i >(1 * rect.right / 2); i--)
	{
		MoveToEx(hDC, (3 * rect.right / 5), (rect.bottom / 3), NULL);
		LineTo(hDC, i, (rect.bottom / 3));
		Sleep(10);
	}

	for (i = (rect.bottom / 3); i > rect.bottom / 6; i--)
	{
		MoveToEx(hDC, (1 * rect.right / 2), (rect.bottom / 3), NULL);
		LineTo(hDC, (1 * rect.right / 2), i);
		Sleep(10);
	}

	/*Points to draw first Arc*/
	POINT apt[4];
	apt[0].x = (3 * rect.right / 5);
	apt[0].y = rect.bottom / 6;
	apt[1].x = (13 * rect.right / 20);
	apt[1].y = (3 * rect.bottom / 12);
	apt[2].x = (13 * rect.right / 20);
	apt[2].y = (5 * rect.bottom / 12);
	apt[3].x = (3 * rect.right / 5);
	apt[3].y = rect.bottom / 2;
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	/*Drawing second Arc*/
	apt[0].x = (3 * rect.right / 5);
	apt[0].y = rect.bottom / 3;
	apt[1].x = (13 * rect.right / 20);
	apt[1].y = (5 * rect.bottom / 12);
	apt[2].x = (13 * rect.right / 20);
	apt[2].y = (7 * rect.bottom / 12);
	apt[3].x = (3 * rect.right / 5);
	apt[3].y = (2 * rect.bottom / 3);
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	ReleaseDC(hwnd, hDC);
	DeleteObject(hOrangePen);
	return(0);
}

DWORD WINAPI MyThreadThree(LPVOID Param)
{
	HWND hwnd;
	hwnd = (HWND)Param;
	HPEN hOrangePen;
	HDC hDC;
	int i;
	RECT rect;
	DWORD nPt = 4;

	hDC = GetDC(hwnd);
	hOrangePen = CreatePen(PS_SOLID, 2, RGB(255, 200, 0));
	SelectObject(hDC, hOrangePen);
	SelectObject(hDC, (HBRUSH)BLACK_BRUSH);
	GetClientRect(hwnd, &rect);
	//InvalidateRect(hwnd, &rect, FALSE);

	for (i = (3 * rect.right / 10); i < (2 * rect.right / 5); i++)
	{
		MoveToEx(hDC, (3 * rect.right / 10), (1 * rect.bottom / 2), NULL);
		LineTo(hDC, i, (1 * rect.bottom / 2));
		Sleep(10);
	}

	for (i = (1 * rect.bottom / 2); i < (2 * rect.bottom / 3); i++)
	{
		MoveToEx(hDC, (2 * rect.right / 5), (1 * rect.bottom / 2), NULL);
		LineTo(hDC, (2 * rect.right / 5), i);
		Sleep(10);
	}

	for (i = (2 * rect.right / 5); i >(3 * rect.right / 10); i--)
	{
		MoveToEx(hDC, (2 * rect.right / 5), (2 * rect.bottom / 3), NULL);
		LineTo(hDC, i, (2 * rect.bottom / 3));
		Sleep(10);
	}

	for (i = (2 * rect.bottom / 3); i > (1 * rect.bottom / 2); i--)
	{
		MoveToEx(hDC, (3 * rect.right / 10), (2 * rect.bottom / 3), NULL);
		LineTo(hDC, (3 * rect.right / 10), i);
		Sleep(10);
	}

	/*Points to draw first Arc*/
	POINT apt[4];
	apt[0].x = (3 * rect.right / 10);
	apt[0].y = (2 * rect.bottom / 3);
	apt[1].x = (7 * rect.right / 20);
	apt[1].y = (9 * rect.bottom / 12);
	apt[2].x = (9 * rect.right / 20);
	apt[2].y = (3 * rect.bottom / 4);
	apt[3].x = rect.right / 2;
	apt[3].y = (2 * rect.bottom / 3);
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	/*Points to draw second Arc*/
	apt[0].x = (2 * rect.right / 5);
	apt[0].y = (2 * rect.bottom / 3);
	apt[1].x = (9 * rect.right / 20);
	apt[1].y = (3 * rect.bottom / 4);
	apt[2].x = (11 * rect.right / 20);
	apt[2].y = (3 * rect.bottom / 4);
	apt[3].x = (3 * rect.right / 5);
	apt[3].y = (2 * rect.bottom / 3);
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	ReleaseDC(hwnd, hDC);
	DeleteObject(hOrangePen);
	return(0);
}

DWORD WINAPI MyThreadFour(LPVOID Param)
{
	HWND hwnd;
	hwnd = (HWND)Param;
	HPEN hOrangePen;
	HDC hDC;
	int i;
	RECT rect;
	DWORD nPt = 4;

	hDC = GetDC(hwnd);
	hOrangePen = CreatePen(PS_SOLID, 2, RGB(255, 200, 0));
	SelectObject(hDC, hOrangePen);
	SelectObject(hDC, (HBRUSH)BLACK_BRUSH);
	GetClientRect(hwnd, &rect);
	//InvalidateRect(hwnd, &rect, FALSE);

	for (i = (1 * rect.right / 2); i < (3 * rect.right / 5); i++)
	{
		MoveToEx(hDC, (1 * rect.right / 2), (1 * rect.bottom / 2), NULL);
		LineTo(hDC, i, (1 * rect.bottom / 2));
		Sleep(10);
	}

	for (i = (1 * rect.bottom / 2); i < (2 * rect.bottom / 3); i++)
	{
		MoveToEx(hDC, (3 * rect.right / 5), (1 * rect.bottom / 2), NULL);
		LineTo(hDC, (3 * rect.right / 5), i);
		Sleep(10);
	}

	for (i = (3 * rect.right / 5); i >(1 * rect.right / 2); i--)
	{
		MoveToEx(hDC, (3 * rect.right / 5), (2 * rect.bottom / 3), NULL);
		LineTo(hDC, i, (2 * rect.bottom / 3));
		Sleep(10);
	}

	for (i = (2 * rect.bottom / 3); i > (1 * rect.bottom / 2); i--)
	{
		MoveToEx(hDC, (1 * rect.right / 2), (2 * rect.bottom / 3), NULL);
		LineTo(hDC, (1 * rect.right / 2), i);
		Sleep(10);
	}

	/*Points to draw first Arc*/
	POINT apt[4];
	apt[0].x = (3 * rect.right / 10);
	apt[0].y = rect.bottom / 6;
	apt[1].x = rect.right / 4;
	apt[1].y = (3 * rect.bottom / 12);
	apt[2].x = rect.right / 4;
	apt[2].y = (5 * rect.bottom / 12);
	apt[3].x = (3 * rect.right / 10);
	apt[3].y = rect.bottom / 2;
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	/*Drawing second Arc*/
	apt[0].x = (3 * rect.right / 10);
	apt[0].y = rect.bottom / 3;
	apt[1].x = rect.right / 4;
	apt[1].y = (5 * rect.bottom / 12);
	apt[2].x = rect.right / 4;
	apt[2].y = (7 * rect.bottom / 12);
	apt[3].x = (3 * rect.right / 10);
	apt[3].y = (2 * rect.bottom / 3);
	/*Drawing an Arc*/
	PolyBezier(hDC, apt, 4);

	ReleaseDC(hwnd, hDC);
	DeleteObject(hOrangePen);
	return(0);
}

DWORD WINAPI MyAudioThread(LPVOID)
{
	PlaySound(MAKEINTRESOURCE(MYAUDIO), GetModuleHandle(NULL), SND_RESOURCE);
	return(0);
}

void ThreadStart()
{
	ResumeThread(hThread_One);
	ResumeThread(hThread_Two);
	ResumeThread(hThread_Three);
	ResumeThread(hThread_Four);
	ResumeThread(hThreadAudio);
}