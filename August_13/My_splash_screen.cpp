/*Include headers*/
#include<tchar.h>
#include"MyResource.h"

/*Global function declaration
Function Prototype */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*Dialog box callback function*/
BOOL  CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	/*Variable declaration*/
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	/* TCHAR(In C) is UNICODE style 2 bytes per character. Range: 0-65536 */
	TCHAR szAppName[] = TEXT("My App");

	/*Flush the wndclass structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	/*Initialization of WNDCLASSEX*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	/*Register this callback function to class*/
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	/*Class name should be unique and it is hashed by OS by Appending/ Prepending some characters*/
	wndclass.lpszClassName = szAppName;
	/*No menu in our application*/
	wndclass.lpszMenuName = NULL;
	/*Application Icon when it is minimized. Here it is kept default*/
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create Winndow*/
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	SetClassLong(hwnd, GCL_HICON, (LONG)LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON)));
	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	/*Message Loop: Gives life to Window: Get -> Translate -> Dispatch msg
	GetMessage returns Boolean value (Typedef BOOL), returns FALSE only when msg is WM_QUIT*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC static hdc;
	HDC static hdc_spl;
	PAINTSTRUCT ps;
	HBITMAP static hBitMap;
	BITMAP hBitInfo;
	RECT rc;
	static HINSTANCE instance;
	TCHAR str[] = TEXT("Enter space key to Continue...and Esc to Exit!");
	HFONT hMyFont;
	//HINSTANCE hInst;

	switch (iMsg)
	{
	case WM_CREATE:
		instance = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);
		hBitMap = LoadBitmap(instance, MAKEINTRESOURCE(MY_BITMAP));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			/*hwnd - handle to window and GWL_HINSTANCE- Retrives handle to the application instance*/
			DialogBox(instance, TEXT("DETAILS"), hwnd, (DLGPROC)DlgProc);
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		hdc_spl = CreateCompatibleDC(hdc);
		SelectObject(hdc_spl, hBitMap);
		GetObject(hBitMap, sizeof(hBitInfo), (LPSTR)&hBitInfo);
		BitBlt(hdc, rc.left, rc.top, hBitInfo.bmWidth, hBitInfo.bmHeight, hdc_spl,0, 0, SRCCOPY);
		SetBkMode(hdc, TRANSPARENT);
		SetTextColor(hdc, RGB(255, 255, 255));
		hMyFont = CreateFont(50, 25, 0, 0, FW_NORMAL, 1, 0, 0, MAC_CHARSET, OUT_RASTER_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_ROMAN, TEXT("Impact"));
		SelectObject(hdc, hMyFont);
		TextOut(hdc, 0, 0, str, _tcslen(str));
		DeleteDC(hdc_spl);
		DeleteObject(hMyFont);
		EndPaint(hwnd, &ps);
		
		break;
	case WM_DESTROY:
		DeleteObject(hBitMap);
		/*WM_QUITE*/
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

/*Dialog box procedure*/

BOOL CALLBACK DlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	TCHAR name[50];
	//int  age;
	//char *name1;
	switch (iMsg)
	{
	case WM_INITDIALOG:
		/*Set focus in name edit box*/
		SetFocus(GetDlgItem(hDlg, ID_ETNAME));
		/*Keep maaried radiobutton checked*/
		SendDlgItemMessage(hDlg, ID_RBSTUDENT, BM_SETCHECK, 1, 0);
		return(TRUE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBOK:
			/*Get the user entered name */
			GetDlgItemText(hDlg, ID_ETNAME, name, 50);
			/*Get user entered age*/
			GetDlgItemInt(hDlg, ID_ETAGE, NULL, TRUE);
			/*Get user entered maritial status*/
			SendDlgItemMessage(hDlg, ID_RBSTUDENT, BM_GETCHECK, 0, 0);
			EndDialog(hDlg, wParam);
			return TRUE;
			break;

		case ID_PBCANCEL:
			EndDialog(hDlg, wParam);
			return TRUE;
			break;
		}
		break;
	}
	return FALSE;
}
