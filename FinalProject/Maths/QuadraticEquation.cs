using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace QuadraticEquation
{	
	[ClassInterface(ClassInterfaceType.AutoDual)]
	public class QuadraticEquation
	{
		public double EquationFactorOne { get; set; }
		public double EquationFactorTwo { get; set; }
		public QuadraticEquation()
		{
			//empty
		}
		public double SolveQuadraticEquationFactorOne(int a, int b, int c)
		{
			double num = ((Math.Pow(b, 2)) - (4 * a * c)); 
			EquationFactorOne = ( ( (-b) + Math.Sqrt(num) ) / (2 * a) ); 
			return ( ( (-b) + Math.Sqrt(num) ) / (2 * a) );
		}
				
		public double SolveQuadraticEquationFactorTwo(int a, int b, int c)
		{
			double num1 = ((Math.Pow(b, 2)) - (4 * a * c)); 
			EquationFactorTwo = ( ( (-b) - Math.Sqrt(num1) ) / (2 * a) ); 
			return ( ( (-b) - Math.Sqrt(num1) ) / (2 * a) );
		}
			
	}
}
