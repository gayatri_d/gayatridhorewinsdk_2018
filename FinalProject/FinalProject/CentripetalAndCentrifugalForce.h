#include<windows.h>
#include<math.h>
#include<tchar.h>

#include"CalculateNumberOfMoleculesInSubstance.h"

extern "C" double CalculateCentripetalForce(double, double, double);
extern "C" double CalculateCentrifugalAcceleration(double, double);
extern "C" void CalculteNumberOfMolecules(double, double, double*);
