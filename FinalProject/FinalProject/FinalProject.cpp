/*Include headers*/
#include<tchar.h>
#include<wchar.h>
#include"FinalProject.h"
#import "QuadraticEquation.tlb" no_namespace, raw_interfaces_only
#include"Debug\quadraticequation.tlh"

/*Global function declaration
Function Prototype */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK ChildProc(HWND, UINT, WPARAM, LPARAM);
/*CalculateCentripetalForce function*/
typedef double(*pfnCalculateCentripetalForce)(double, double, double);
pfnCalculateCentripetalForce pfn;

/*CalculateCentrifugalAcceleration function*/
typedef double(*pfnCalculateCentrifugalAcceleration)(double, double);
pfnCalculateCentrifugalAcceleration pfn_centrifugalAcc;

/*CalculteNumberOfMolecules function*/
typedef void(*pfnCalculteNumberOfMolecules)(double, double, double*); 
pfnCalculteNumberOfMolecules pfn_NMolecules;

/*CalculateFirstFactor function*/
/*typedef double(*pfnCalculateFirstFactor)(int, int, int);
pfnCalculateFirstFactor pfn_firstFactor;

/*CalculateSecondFactor function*/
/*typedef double(*pfnCalculateSecondFactor)(int, int, int);
pfnCalculateSecondFactor pfn_secondFactor;*/

/*Physics Dialog box callback function*/
BOOL  CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);

/*Chemistry Dialog Box callback function*/
BOOL CALLBACK ChemDlgProc(HWND, UINT, WPARAM, LPARAM);

/*MAths Dialog Box callback function*/
BOOL CALLBACK MathsDlgProc(HWND, UINT, WPARAM, LPARAM);

/*Function to write data to file*/
bool WriteDataToFile(LPCWSTR, char *);

/*Global variable declaration*/
HWND hwndComboBox;
static _QuadraticEquation *app;
CLSID clsidMath;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	/*Variable declaration*/
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	/* TCHAR(In C) is UNICODE style 2 bytes per character. Range: 0-65536 */
	TCHAR szAppName[] = TEXT("My App");
	HRESULT hrCOM;
	hrCOM = CoInitialize(NULL);
	if (FAILED(hrCOM)) {
		MessageBox(NULL, TEXT("COM Library Can Not Be Initialized\nProgram Will Now Exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}

	/*Flush the wndclass structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	/*Initialization of WNDCLASSEX*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	/*Register this callback function to class*/
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	/*Class name should be unique and it is hashed by OS by Appending/ Prepending some characters*/
	wndclass.lpszClassName = szAppName;
	/*No menu in our application*/
	wndclass.lpszMenuName = NULL;
	/*Application Icon when it is minimized. Here it is kept default*/
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON));

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create Winndow*/
	hwnd = CreateWindow(szAppName,
		TEXT("SDK Project"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	SetClassLong(hwnd, GCL_HICON, (LONG)LoadIcon(hInstance, MAKEINTRESOURCE(MY_ICON)));

	ShowWindow(hwnd, SW_MAXIMIZE);
	UpdateWindow(hwnd);

	/*Message Loop: Gives life to Window: Get -> Translate -> Dispatch msg
	GetMessage returns Boolean value (Typedef BOOL), returns FALSE only when msg is WM_QUIT*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	CoUninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC static hdc;
	HDC static hdc_spl;
	PAINTSTRUCT ps;
	HBITMAP static hBitMap;
	BITMAP hBitInfo;
	static RECT rc;
	static HINSTANCE instance;
	TCHAR str[] = TEXT("Choose any of the following!");
	TCHAR gurukul[] = TEXT("ASTROMEDICOMP");
	TCHAR name[] = TEXT("Gayatri Manoj Dhore");
	TCHAR batch[] = TEXT("WinRT2018");
	TCHAR gName[] = TEXT("WM_APP");
	HFONT hMyFont;
	HINSTANCE hLib = NULL;
	static int ItemIndex;

	instance = (HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE);

	TCHAR *subjects[4] = { TEXT("Physics"), TEXT("Chemistry"), TEXT("Maths"), TEXT("Biology") };

	switch (iMsg)
	{
	case WM_CREATE:
		/*Combo box*/
		HRESULT hr;

		SendMessage(GetDlgItem(hwnd, ID_CB), (UINT)CB_SETCURSEL, (WPARAM)2, (LPARAM)0);
		hLib = LoadLibrary(TEXT("CentripetalAndCentrifugalForce.dll"));
		if (hLib == NULL)
		{
			MessageBox(hwnd, TEXT("Error in loading library"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			DestroyWindow(hwnd);
		}
		pfn = (pfnCalculateCentripetalForce)GetProcAddress(hLib, "CalculateCentripetalForce");
		pfn_centrifugalAcc = (pfnCalculateCentrifugalAcceleration)GetProcAddress(hLib, "CalculateCentrifugalAcceleration");
		pfn_NMolecules = (pfnCalculteNumberOfMolecules)GetProcAddress(hLib, "CalculteNumberOfMolecules");
		
		/*pfn_firstFactor = (pfnCalculateFirstFactor)GetProcAddress(hLib, "CalculateFirstFactor");
		pfn_secondFactor = (pfnCalculateSecondFactor)GetProcAddress(hLib, "CalculateSecondFactor");*/

		/*Maths DLL*/
		hr = CLSIDFromProgID(L"QuadraticEquation.QuadraticEquation", &clsidMath);

		if (FAILED(hr))
		{
			MessageBox(NULL, TEXT("Failed to get CLSID"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}

		hr = CoCreateInstance(clsidMath, NULL, CLSCTX_INPROC_SERVER, __uuidof(_QuadraticEquation), (VOID**)&app);
		
		if (FAILED(hr))
		{
			MessageBox(NULL, TEXT("Failed to load maths dll"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		hBitMap = LoadBitmap(instance, MAKEINTRESOURCE(MY_BITMAP));
		
		/*Create ComboBox as child of main window*/
		/*Get client area*/
		GetClientRect(hwnd, &rc);
		hwndComboBox = CreateWindow(TEXT("combobox"), NULL, CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD, (rc.right / 2) + 150, (rc.bottom / 2) + 90, 80, 100, hwnd, (HMENU)ID_CB, instance, NULL);
		for (int i = 0; i <= 3; i++)
			SendMessage(hwndComboBox, CB_ADDSTRING, 0, (LPARAM)subjects[i]);

		SendMessage(hwndComboBox, CB_SETCURSEL, (WPARAM)2, (LPARAM)0);
		ShowWindow(hwndComboBox, SW_SHOW);
		
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);
		GetClientRect(hwnd, &rc);
		hdc_spl = CreateCompatibleDC(hdc);
		SelectObject(hdc_spl, hBitMap);
		GetObject(hBitMap, sizeof(hBitInfo), (LPSTR)&hBitInfo);
		BitBlt(hdc, rc.left, rc.top, hBitInfo.bmWidth, hBitInfo.bmHeight, hdc_spl, 0, 0, SRCCOPY);
		SetBkMode(hdc, TRANSPARENT);
		SetTextColor(hdc, RGB(255, 255, 255));
		hMyFont = CreateFont(50, 25, 0, 0, FW_NORMAL, 1, 0, 0, MAC_CHARSET, OUT_RASTER_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_ROMAN, TEXT("Impact"));
		SelectObject(hdc, hMyFont);
		TextOut(hdc, (rc.right / 2) - 300, (rc.bottom / 2) - 120, str, _tcslen(str));
		hMyFont = CreateFont(25, 15, 0, 0, FW_THIN, 0, 0, 0, MAC_CHARSET, OUT_RASTER_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_ROMAN, NULL);
		SelectObject(hdc, hMyFont);
		TextOut(hdc, (rc.right / 13),(rc.bottom / 2) + 250, gurukul, _tcslen(gurukul));
		TextOut(hdc, (rc.right / 13), (rc.bottom / 2) + 300, batch, _tcslen(batch));
		TextOut(hdc, (rc.right / 2) + 300, (rc.bottom / 2) + 250, name, _tcslen(name));
		TextOut(hdc, (rc.right / 2) + 300, (rc.bottom / 2) + 300, gName, _tcslen(gName));
		DeleteDC(hdc_spl);
		DeleteObject(hMyFont);
		EndPaint(hwnd, &ps);

		break;

	case WM_COMMAND:
		switch (HIWORD(wParam))
		{
		case CBN_SELCHANGE:
			ItemIndex = SendMessage(hwndComboBox, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
			if (ItemIndex == 0)
				DialogBox(instance, TEXT("PHYSICS"), hwnd, (DLGPROC)DlgProc);
			else if (ItemIndex == 1)
				DialogBox(instance, TEXT("CHEMSIRY"), hwnd, (DLGPROC)ChemDlgProc);
			else if (ItemIndex == 2)
				DialogBox(instance, TEXT("MATHS"), hwnd, (DLGPROC)MathsDlgProc);
			break;
		}
		break;

	case WM_DESTROY:
		DeleteObject(hBitMap);
		DestroyWindow(hwndComboBox);
		FreeLibrary(hLib);	
		/*WM_QUITE*/
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

/*Dialog box procedure*/

BOOL CALLBACK DlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	/*Local variable declration*/
	double mass;
	double velocity;
	double radius;
	double force;
	double acceleration;
	const size_t len = 255;
	char Mass[255] = {0};
	char Velocity[255] = { 0 };
	char Radius[255] = { 0 };
	wchar_t str[len], str1[len];
	static HDC hDC_Dlg;
	static HBRUSH hBrush;
	FILE *fp_dataCP, *fp_dataCA;
	HDC hDCStatic;
	
	

	switch (iMsg)
	{
	case WM_INITDIALOG:
		/*Set focus in mass edit box*/
		SetFocus(GetDlgItem(hDlg, ID_RBCFORCE));
		SendMessage(GetDlgItem(hDlg, ID_RBCFORCE), BM_SETCHECK, BST_CHECKED, 1);
		SendMessage(hDlg, WM_CTLCOLORDLG, (WPARAM)0, (LPARAM)0);
		return(TRUE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBCAL:
			/*Open file to save data*/
			fopen_s(&fp_dataCP, "data_CP.txt", "w+");
			fprintf(fp_dataCP, "Mass\t\t\tVelocity\t\t\tRadius\t\t\tCentripetal Force\n");
			/*Get the user entered mass */
			GetDlgItemTextA(hDlg, ID_ETMASS, Mass, 255);
			mass = atof(Mass);
			fprintf(fp_dataCP, "%lf", mass);
			fprintf(fp_dataCP, "\t\t");
			/*Get the value of Velocity*/
			GetDlgItemTextA(hDlg, ID_ETVELOCITY, Velocity, 255);
			velocity = atof(Velocity);
			fprintf(fp_dataCP,"%lf" ,velocity);
			fprintf(fp_dataCP, "\t\t");
			/*Get the value of radius*/
			GetDlgItemTextA(hDlg, ID_ETRADIUS, Radius, 255);
			radius = atof(Radius);
			fprintf(fp_dataCP, "%lf", radius);
			fprintf(fp_dataCP, "\t\t");
			/*Call to function that calculates force*/
			force = pfn(mass, velocity, radius);
			/*Storing data into string*/
			_snwprintf_s(str, len, L"%lf", force);
			fprintf(fp_dataCP, "%lf", force);
			fprintf(fp_dataCP, "\n");
			fclose(fp_dataCP);
			/*Displaying the result*/
			SetDlgItemText(hDlg, ID_CBFORCE, str);
			return TRUE;
			break;

		case ID_PBCALCA:
			/*Get the value of Velocity*/
			fopen_s(&fp_dataCA, "data_CA.txt", "w+");
			fprintf(fp_dataCA, "Velocity\t\t\tRadius\t\t\tCentrifugal Acceleration\n");
			GetDlgItemTextA(hDlg, ID_ETANGVELO, Velocity, 255);
			velocity = atof(Velocity);
			fprintf(fp_dataCA, "%lf", velocity);
			fprintf(fp_dataCA, "\t\t");
			/*Get the value of radius*/
			GetDlgItemTextA(hDlg, ID_ETRAD, Radius, 255);
			radius = atof(Radius);
			fprintf(fp_dataCA, "%lf", radius);
			fprintf(fp_dataCA, "\t\t");
			/*Calculte centrifugal accelration*/
			acceleration = pfn_centrifugalAcc(velocity, radius);
			fprintf(fp_dataCA, "%lf", acceleration);
			fprintf(fp_dataCA, "\t\t");
			_snwprintf_s(str1, len, L"%lf", acceleration);
			/*Display the result*/
			SetDlgItemText(hDlg, ID_CBACCELE, str1);
			return TRUE;
			break;

		case ID_PBEXIT:
			EndDialog(hDlg, wParam);
			ReleaseDC(NULL,hDC_Dlg);
			return TRUE;
			break;

		case ID_PBEXITCA:
			EndDialog(hDlg, wParam);
			ReleaseDC(NULL, hDC_Dlg);
			return TRUE;
			break;

		case ID_RBCFORCE:
			SendDlgItemMessage(hDlg, ID_RBCACCELE, BM_SETCHECK, (WPARAM)0, (LPARAM)0);
			EnableWindow(GetDlgItem(hDlg, ID_ETANGVELO), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_ETRAD), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_CBACCELE), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_PBEXITCA), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_PBCALCA), FALSE);
			/*Its own TRUE*/
			EnableWindow(GetDlgItem(hDlg, ID_CBFORCE), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_ETMASS), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_ETVELOCITY), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_ETRADIUS), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_PBEXIT), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_PBCAL), TRUE);
			return(TRUE);
			break;

		case ID_RBCACCELE:
			SendDlgItemMessage(hDlg, ID_RBCFORCE, BM_SETCHECK, (WPARAM)0, (LPARAM)0);
			EnableWindow(GetDlgItem(hDlg, ID_CBACCELE), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_ETMASS), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_ETVELOCITY), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_ETRADIUS), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_CBFORCE), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_PBEXIT), FALSE);
			EnableWindow(GetDlgItem(hDlg, ID_PBCAL), FALSE);
			
			/*Its own TRUE*/
			EnableWindow(GetDlgItem(hDlg, ID_ETANGVELO), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_ETRAD), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_CBACCELE), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_PBEXITCA), TRUE);
			EnableWindow(GetDlgItem(hDlg, ID_PBCALCA), TRUE);
			return TRUE;
			break;

		case ID_CB:
			
			break;
		}
		break;

	case WM_CTLCOLORDLG:
		hDC_Dlg = GetDC(hDlg);
		hBrush = (HBRUSH)CreateSolidBrush(RGB(67, 135, 135));
		SetBkColor(hDC_Dlg, (LONG)SelectObject(hDC_Dlg, hBrush));
		return (INT_PTR)((LONG)SelectObject(hDC_Dlg, hBrush));
		break;

	case WM_CTLCOLORSTATIC:
		hDCStatic = (HDC)wParam;
		SetBkColor(hDCStatic, RGB(67, 135, 135));
		SetTextColor(hDCStatic, RGB(0, 0, 0));
		return (INT_PTR)(hBrush);

	}
	return FALSE;
}

/*Chemistry Dilog box control*/

BOOL CALLBACK ChemDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	double MassOfSub;
	double AtMass;
	double Molecules;

	char mass[255];
	char atmass[255];

	const size_t len = 255;
	wchar_t buffer[len];

	static HDC hDC_Dlg;
	static HBRUSH hBrush;
	HDC hDCStatic;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		SetFocus(GetDlgItem(hDlg, ID_ETSUBMASS));
		return(TRUE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBCHEMCAL:
			GetDlgItemTextA(hDlg, ID_ETSUBMASS, mass, 255);
			MassOfSub = atof(mass);
			GetDlgItemTextA(hDlg, ID_ETATMASS, atmass, 255);
			AtMass = atof(atmass);
			pfn_NMolecules(MassOfSub, AtMass, &Molecules);
			_snwprintf_s(buffer, len, L"%lf", Molecules);
			SetDlgItemText(hDlg, ID_ETMOLECULES, buffer);
			return TRUE;
			break;

		case ID_PBCHEMEXIT:
			EndDialog(hDlg, wParam);
			return TRUE;
			break;
		}
		break;

	case WM_CTLCOLORDLG:
		hDC_Dlg = GetDC(hDlg);
		hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 217));
		SetBkColor(hDC_Dlg, (LONG)SelectObject(hDC_Dlg, hBrush));
		return (INT_PTR)((LONG)SelectObject(hDC_Dlg, hBrush));
		break;

	case WM_CTLCOLORSTATIC:
		hDCStatic = (HDC)wParam;
		SetBkColor(hDCStatic, RGB(255, 255, 217));
		SetTextColor(hDCStatic, RGB(0, 0, 0));
		return (INT_PTR)(hBrush);
		break;
	}
	return (FALSE);
}

BOOL CALLBACK MathsDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	int iACoeff;
	int iBCoeff;
	int iCCoeff;
	double firstFactor;
	double secondFactor;

	const size_t len = 255;
	wchar_t buffer[len];
	static HDC hDC_Dlg;
	static HBRUSH hBrush;
	HDC hDCStatic;
	BOOL bSuccess;
	LPCWSTR file_name = L"data.xml";
	char *fileDataWrite = new char[1024];
	size_t size = 100;
	char value[100];
	char *a;

	switch (iMsg)
	{
	case WM_INITDIALOG:
		/*Set focus in mass edit box*/
		SetFocus(GetDlgItem(hDlg, ID_ETACOEFF));
		return(TRUE);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_PBMCAL:
			iACoeff = GetDlgItemInt(hDlg, ID_ETACOEFF, &bSuccess, TRUE);
			iBCoeff = GetDlgItemInt(hDlg, ID_ETBCOEFF, &bSuccess, TRUE);
			iCCoeff = GetDlgItemInt(hDlg, ID_ETCCOEFF, &bSuccess, TRUE);
			
			app->SolveQuadraticEquationFactorOne(iACoeff, iBCoeff, iCCoeff, &firstFactor);
			app->SolveQuadraticEquationFactorTwo(iACoeff, iBCoeff, iCCoeff, &secondFactor);
	
			_snwprintf_s(buffer, len, L"%lf", firstFactor);
			SetDlgItemText(hDlg, ID_FACTORONE, buffer);
			_snwprintf_s(buffer, len, L"%lf", secondFactor);
			SetDlgItemText(hDlg, ID_FACTORTWO, buffer);
			
			/*Storing data to file*/
			strcpy_s(fileDataWrite, 1024, "<Maths>\n\t");
			strcat_s(fileDataWrite, 1024, "<QuadraticEquation>\n\t");
			strcat_s(fileDataWrite, 1024, "<a>\n\t");
			_itoa_s(iACoeff, value, size, 10);
			strcat_s(fileDataWrite, 1024, value);
			strcat_s(fileDataWrite, 1024, "\n\t</a>\n\t");
			strcat_s(fileDataWrite, 1024, "<b>\n\t");
			_itoa_s(iBCoeff, value, size, 10);
			strcat_s(fileDataWrite, 1024, value);
			strcat_s(fileDataWrite, 1024, "\n\t</b>\n\t");
			strcat_s(fileDataWrite, 1024, "<c>\n\t");
			_itoa_s(iCCoeff, value, size, 10);
			strcat_s(fileDataWrite, 1024, value);
			strcat_s(fileDataWrite, 1024, "\n\t</c>\n\t");
			strcat_s(fileDataWrite, 1024, "</QuadraticEquation>\n\t");
			strcat_s(fileDataWrite, 1024, "<FirstFactor>\n\t");
			sprintf_s(value, "%lf", firstFactor);
			strcat_s(fileDataWrite, 1024, value);
			strcat_s(fileDataWrite, 1024, "\n\t</FirstFactor>\n\t");
			strcat_s(fileDataWrite, 1024, "<SecondFactor>\n\t");
			sprintf_s(value, "%lf", secondFactor);
			strcat_s(fileDataWrite, 1024, value);
			strcat_s(fileDataWrite, 1024, "\n\t</SecondFactor>\n");
			strcat_s(fileDataWrite, 1024, "</Maths>\n");
			WriteDataToFile(file_name, fileDataWrite);
			return TRUE;
			break;

		case ID_PBMEXIT:
			EndDialog(hDlg, wParam);
			return TRUE;
			break;
		}
		break;

	case WM_CTLCOLORDLG:
		hDC_Dlg = GetDC(hDlg);
		hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 217));
		SetBkColor(hDC_Dlg, (LONG)SelectObject(hDC_Dlg, hBrush));
		return (INT_PTR)((LONG)SelectObject(hDC_Dlg, hBrush));
		break;

	case WM_CTLCOLORSTATIC:
		hDCStatic = (HDC)wParam;
		SetBkColor(hDCStatic, RGB(255, 255, 217));
		SetTextColor(hDCStatic, RGB(0, 0, 0));
		return (INT_PTR)(hBrush);
		break;
	}

	return (FALSE);
}

bool WriteDataToFile(LPCWSTR fileName, char* Data)
{
	HANDLE hFile;
	BOOL bErrorFlag = FALSE;
	DWORD dwBytesToWrite = (DWORD)strlen(Data);
	DWORD dwByteWritten = 0;

	/*Copying header at start of file*/
	hFile = CreateFile(fileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		char *xmlFileHeader = new char[100];
		strcpy_s(xmlFileHeader, 100, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<xml>\n</xml>\n");
		DWORD dwBytesToWriteNew = (DWORD)strlen(xmlFileHeader);
		DWORD dwByteWrittenNew = 0;

		bErrorFlag = WriteFile(hFile, xmlFileHeader, dwBytesToWriteNew, &dwByteWrittenNew, NULL);
	}
	CloseHandle(hFile);

	/*Copying actual data to file*/
	hFile = CreateFile(fileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	SetFilePointer(hFile, -7, NULL, FILE_END);
	if (hFile != INVALID_HANDLE_VALUE)
	{
		bErrorFlag = WriteFile(hFile, Data, dwBytesToWrite, &dwByteWritten, NULL);
	}

	/*Copying end tag to file*/
	char *xmlFileHeader = new char[10];
	strcpy_s(xmlFileHeader, 10, "</xml>\n");
	DWORD dwBytesToWriteNew = (DWORD)strlen(xmlFileHeader);
	DWORD dwBytesWrittenNew = 0;

	bErrorFlag = WriteFile(
		hFile,           // open file handle
		xmlFileHeader,      // start of data to write
		dwBytesToWriteNew,  // number of bytes to write
		&dwBytesWrittenNew, // number of bytes that were written
		NULL);            // no overlapped structure

	CloseHandle(hFile);
	return bErrorFlag;
}
