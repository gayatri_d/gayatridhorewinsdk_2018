#include<windows.h>
#include<math.h>

class IMolecules : public IUnknown
{
public:
	virtual HRESULT __stdcall CalculteNumberOfMolecules(double, double, double*) = 0;
};

/*CLSID of class implementing these two interfaces*/
// {3557FDED-56FD-4FC1-A216-BFCC8016FEBB}
const CLSID CLSID_Molecules = { 0x3557fded, 0x56fd, 0x4fc1,{ 0xa2, 0x16, 0xbf, 0xcc, 0x80, 0x16, 0xfe, 0xbb } };

/*IID of ISquare interface*/
// {7F8FE328-6F6C-4E6B-8836-6008DFC64A28}
const IID IID_IMolecules = { 0x7f8fe328, 0x6f6c, 0x4e6b,{ 0x88, 0x36, 0x60, 0x8, 0xdf, 0xc6, 0x4a, 0x28 } };
