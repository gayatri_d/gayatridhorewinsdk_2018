#include<Windows.h>
#include<iostream>
#include<strsafe.h>
#include<string>

/*ID for ICON*/
#define MY_ICON				100
/*ID for Bitmap image*/
#define MY_BITMAP			101

/*Centripetal force RadioButton*/
#define ID_RBCFORCE			102
/*Centrifugal Acceleration RadioButton*/
#define ID_RBCACCELE		103

/*Centripetal force parameters mass, velocit, radius*/
#define ID_ETMASS			104
#define ID_ETVELOCITY		105
#define ID_ETRADIUS		    106
#define ID_CBFORCE		    107

/*Centrifugal acceleration parameters*/
#define ID_ETANGVELO		108
#define ID_ETRAD			109
#define ID_CBACCELE			110

/*Control buttons for Centripetal force*/
#define ID_PBEXIT			111
#define ID_PBCAL			112

/*Control Buttons for Centrifugal acceleration*/
#define ID_PBEXITCA			113
#define ID_PBCALCA			114

/*Combo box ID*/
#define ID_CB				115

/*Chemsitry controls*/
#define ID_ETSUBMASS		116
#define ID_ETATMASS			117
#define ID_ETMOLECULES		118
#define ID_PBCHEMCAL		119
#define ID_PBCHEMEXIT		120

/*Maths Controls*/
#define ID_ETACOEFF			121
#define ID_ETBCOEFF			122
#define ID_ETCCOEFF			123
#define ID_FACTORONE		124
#define ID_FACTORTWO		125
#define ID_PBMCAL			126
#define ID_PBMEXIT			127
