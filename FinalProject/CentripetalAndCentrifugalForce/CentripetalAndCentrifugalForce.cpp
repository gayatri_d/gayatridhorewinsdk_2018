#include"CentripetalAndCentrifugalForce.h"

/*Global varibales*/
static IMolecules *pIMolecules;
HRESULT hr;

/*Entry Point fnction*/
BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	/* Following cases indicates why this dll is loaded/ unloaded */
	switch (dwReason)
	{
		/*case 1: DLL is loaded because DLL is attached to process*/
	case DLL_PROCESS_ATTACH:
		hr = CoCreateInstance(CLSID_Molecules, NULL, CLSCTX_INPROC_SERVER, IID_IMolecules, (void**)&pIMolecules);
		if (FAILED(hr))
		{
			MessageBox(NULL, TEXT("IMolecule interface can not be loaded"), TEXT("Error"), MB_OK);
		}
		break;

		/*case 2: DLL is loaded because DLL is attached to thread*/
	case DLL_THREAD_ATTACH:
		break;

		/*case 3: DLL is unloaded because thread detached it*/
	case  DLL_THREAD_DETACH:
		pIMolecules->Release();
		pIMolecules = NULL;
		break;

		/*case 4: DLL is unloaded because process detached it*/
	case DLL_PROCESS_DETACH:
		break;

	}
	return TRUE;
}


extern "C" double CalculateCentripetalForce(double mass, double velocity, double radius)
{
	double Force;
	Force = (mass *(velocity * velocity)) / (radius);
	return(Force);
}

extern "C" double CalculateCentrifugalAcceleration(double velocity, double radius)
{
	return((velocity * velocity) / radius);
}

extern "C" void CalculteNumberOfMolecules(double MassIngms, double AtomicMass, double *NumberOfAtoms)
{
	/*hr = pIMolecules->QueryInterface(IID_IMolecules, (void**)&pIMolecules);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IMolecules interface can not be obtained"), TEXT("ERROR"), MB_OK);
	}*/
	pIMolecules->CalculteNumberOfMolecules(MassIngms, AtomicMass, NumberOfAtoms);
}
