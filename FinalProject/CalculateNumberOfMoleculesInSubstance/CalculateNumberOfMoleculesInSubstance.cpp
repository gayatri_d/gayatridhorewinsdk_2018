#include"CalculateNumberOfMoleculesInSubstance.h"

/*Global varible declaration*/
long glNumberOfActiveComponents = 0;
long glNumberOfServerLock = 0;

/*Decelration of class implementing intefaces*/
class CMolecules : public IMolecules
{
private:
	long m_cRef;
public:
	/*Constructor method*/
	CMolecules();
	/*Destructor Method*/
	~CMolecules();

	/*IUnknown Specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*IMolecules specific method*/
	HRESULT __stdcall CalculteNumberOfMolecules(double, double, double*);
};

class CMoleculesClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method*/
	CMoleculesClassFactory(void);
	/*Desturctor Method*/
	~CMoleculesClassFactory(void);
	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*IClassFactory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*Dll main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID lpReaserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Constructor method of CSquareCube*/
CMolecules::CMolecules()
{
	m_cRef = 1;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

/*Destructor method*/
CMolecules :: ~CMolecules()
{
	InterlockedDecrement(&glNumberOfActiveComponents);
}

/*QueryInterface*/
HRESULT CMolecules::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IMolecules *>(this);
	else if (riid == IID_IMolecules)
		*ppv = static_cast<IMolecules *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

/*AddRef() method*/
ULONG CMolecules::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

/*Release() method*/
ULONG CMolecules::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*ISqure method*/
HRESULT CMolecules::CalculteNumberOfMolecules(double AtomicMassIngms, double AtomicMass, double *NumberofMolecules)
{
	*NumberofMolecules = ((6.022 * pow(10, 23)) / AtomicMass) * (AtomicMassIngms);
	return(S_OK);
}

/*CSquareCubeClassFactory constructor*/
CMoleculesClassFactory::CMoleculesClassFactory(void)
{
	m_cRef = 1;
}

CMoleculesClassFactory ::~CMoleculesClassFactory(void)
{
	//code
}

HRESULT CMoleculesClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMoleculesClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMoleculesClassFactory::Release()
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of CSumSubtractClassFactory's IClassFactory's Method*/
HRESULT CMoleculesClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	//variable declarations
	CMolecules *pCMolecules = NULL;
	HRESULT hr;

	//code
	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	//create the instance of component i.e of CSumSubtract class
	pCMolecules = new CMolecules;

	if (pCMolecules == NULL)
		return(E_OUTOFMEMORY);
	//get the requested interface 
	hr = pCMolecules->QueryInterface(riid, ppv);
	pCMolecules->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT CMoleculesClassFactory::LockServer(BOOL fLock)
{
	//code
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLock);
	else
		InterlockedDecrement(&glNumberOfServerLock);

	return(S_OK);
}

/*Implementation of Exported functions from this dll*/
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	//variable declration
	CMoleculesClassFactory *pCMoleculesClassFactory = NULL;
	HRESULT hr;
	//code
	if (rclsid != CLSID_Molecules)
		return(CLASS_E_CLASSNOTAVAILABLE);
	//Create class factory
	pCMoleculesClassFactory = new CMoleculesClassFactory();
	if (pCMoleculesClassFactory == NULL)
		return(E_OUTOFMEMORY);

	hr = pCMoleculesClassFactory->QueryInterface(riid, ppv);

	pCMoleculesClassFactory->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	//code
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLock == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
