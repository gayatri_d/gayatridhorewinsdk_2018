/*Include headers */
#include<Windows.h>
#include<tchar.h>
#include<wchar.h>
#include<stdio.h>

/*Function Prototype of Window Procedure */
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

/*WinMain*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	/*Variable declaration */
	WNDCLASSEX wndclass;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("My App");
	MSG msg;

	/*Flushing the class structure */
	memset((void*)&wndclass, 0, sizeof(WNDCLASSEX));

	/*Initializing the class members*/
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.hInstance = hInstance;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;

	/*Register class to OS*/
	RegisterClassEx(&wndclass);

	/*Create Window*/
	hwnd = CreateWindow(szAppName,
		TEXT("Message Handling"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	/*Update window - First time WM_PAINT message is sent */
	UpdateWindow(hwnd);

	/*Message loop*/
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM  wParam, LPARAM lParam)
{
	HINSTANCE hLib = NULL;
	int num = 0;
	double result;
	float output;

	int number1 = 10;
	int number2 = 7;
	/*MakeSquare function*/
	typedef int(*pfnMakeSquare)(int);
	pfnMakeSquare pfn = NULL;
	/*Divison function*/
	typedef double(*pfndivison)(int, int);
	pfndivison pfnd = NULL;
	/*Divide by four function*/
	typedef float(*pfndfour)(int);
	pfndfour pfnfour = NULL;

	const size_t len = 255;
	wchar_t str[len];
	wchar_t msg[len] = TEXT("Divison");
	switch (iMsg)
	{
		/*Message 1*/
	case WM_CREATE:
		hLib = LoadLibrary(TEXT("Square_DEF_Dll.dll"));
		if (hLib == NULL)
		{
			MessageBox(hwnd, TEXT("Error in loading library"), TEXT("Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);
			DestroyWindow(hwnd);
		}
		pfn = (pfnMakeSquare)GetProcAddress(hLib, "MakeSquare");
		num = pfn(number1);
		_snwprintf_s(str, len, L"Square of %d is %d", number1, num);
		MessageBox(hwnd, str, TEXT("MakeSquare Output"), MB_OK | MB_ICONINFORMATION | MB_TOPMOST);

		pfnd = (pfndivison)GetProcAddress(hLib, "Divison");
		result = pfnd(number1, number2);
		_snwprintf_s(str, len, L"Divison of %d and %d is %lf", number1, number2, result);
		MessageBox(hwnd, str, TEXT("Divison Output"), MB_OK | MB_ICONINFORMATION | MB_TOPMOST);

		pfnfour = (pfndfour)GetProcAddress(hLib, "DivideByfour");
		output = pfnfour(number1);
		_snwprintf_s(str, len, L"Divison of %d by 4 is %f", number1, output);
		MessageBox(hwnd, str, TEXT("Divison Output"), MB_OK | MB_ICONINFORMATION | MB_TOPMOST);
		FreeLibrary(hLib);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}