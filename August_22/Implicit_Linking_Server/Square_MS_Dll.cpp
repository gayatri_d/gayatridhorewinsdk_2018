#include<Windows.h>
#include"Square_MS_Dll.h"

/*Entry Point fnction*/
BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	/* Following cases indicates why this dll is loaded/ unloaded */
	switch (dwReason)
	{
		/*case 1: DLL is loaded because DLL is attached to process*/
	case DLL_PROCESS_ATTACH:
		break;
		
		/*case 2: DLL is loaded because DLL is attached to thread*/
	case DLL_THREAD_ATTACH:
		break;

		/*case 3: DLL is unloaded because thread detached it*/
	case  DLL_THREAD_DETACH:
		break;

		/*case 4: DLL is unloaded because process detached it*/
	case DLL_PROCESS_DETACH:
		break;

	}
	return TRUE;
}


extern "C" __declspec(dllexport) int MakeSquare(int num)
{
	return(num * num);
}
