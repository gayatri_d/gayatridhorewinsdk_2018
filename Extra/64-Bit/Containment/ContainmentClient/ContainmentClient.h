#include<windows.h>

class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class ISubtract : public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; //pure virtulal
};

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual 
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of CLSID_SumSubtract*/
// {FBD800EC-9673-4409-8112-53AC63B3BBAD}
const CLSID CLSID_SumSubtract = { 0xfbd800ec, 0x9673, 0x4409,{ 0x81, 0x12, 0x53, 0xac, 0x63, 0xb3, 0xbb, 0xad } };

/*IID of ISum Interface*/
// {6253A59A-DC63-44AA-9114-23CC0E025240}
const IID IID_ISum = { 0x6253a59a, 0xdc63, 0x44aa,{ 0x91, 0x14, 0x23, 0xcc, 0xe, 0x2, 0x52, 0x40 } };

/*IID of ISubtract Interface*/
// {DED8DC98-40B7-4656-9F8A-9D0761CCD137}
const IID IID_ISubtract = { 0xded8dc98, 0x40b7, 0x4656,{ 0x9f, 0x8a, 0x9d, 0x7, 0x61, 0xcc, 0xd1, 0x37 } };

/*IID of IMultiplication interface*/
// {7BA634A0-5F2D-45E7-87B8-6FEB15AE3B8D}
const IID IID_IMultiplication = { 0x7ba634a0, 0x5f2d, 0x45e7,{ 0x87, 0xb8, 0x6f, 0xeb, 0x15, 0xae, 0x3b, 0x8d } };

/*IID of IDivison interface*/
// {6719502E-7062-4E5F-BF25-1FD10D6B2477}
const IID IID_IDivison = { 0x6719502e, 0x7062, 0x4e5f,{ 0xbf, 0x25, 0x1f, 0xd1, 0xd, 0x6b, 0x24, 0x77 } };


