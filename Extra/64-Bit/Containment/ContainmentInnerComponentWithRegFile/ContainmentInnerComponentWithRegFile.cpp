/*Inner component gives facility of Multiplication and divison*/

/*Global Variables*/
long glNumbrOfActiveComponants;
long glNumberOfServerLocks;

/*Includes*/
#include"ContainmentInnerComponentWithRegFile.h"

/*Co-class*/
class CMultiplicationDivison : public IMultiplication, IDivison
{

private:
	long m_cRef;
public:
	/*Constructor method declration*/
	CMultiplicationDivison(void);
	/*Destructor method declaration*/
	~CMultiplicationDivison(void);
	/*IUnknown methods declaration*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*IMultiplication specific method*/
	HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*);

	/*IDivison specfic methods*/
	HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*);
};

/*Class Factory*/
class CMultipllicationDivisonClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method declaration*/
	CMultipllicationDivisonClassFactory(void);
	/*Destructor method declaration*/
	~CMultipllicationDivisonClassFactory(void);
	/*IUnknown method declration*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*IClass Factory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown *, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*Dll Main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}
/*Implementation of Co-class*/
CMultiplicationDivison::CMultiplicationDivison(void)
{
	m_cRef = 1;
	InterlockedIncrement(&glNumbrOfActiveComponants);
}

CMultiplicationDivison::~CMultiplicationDivison(void)
{
	InterlockedDecrement(&glNumbrOfActiveComponants);
}

HRESULT CMultiplicationDivison::QueryInterface(REFIID riid, void ** ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IMultiplication *>(this);
	else if (riid == IID_IMultiplication)
		*ppv = static_cast<IMultiplication *>(this);
	else if (riid == IID_IDivison)
		*ppv = static_cast<IDivison *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMultiplicationDivison::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMultiplicationDivison::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of IMultiplication specific method*/
HRESULT CMultiplicationDivison::MultiplicationOfTwoIntegers(int num1, int num2, int *pSum)
{
	*pSum = num1 * num2;
	return(S_OK);
}

/*Implementation of IDivison specific method*/
HRESULT CMultiplicationDivison::DivisonOfTwoIntegers(int num1, int num2, int *pDiv)
{
	*pDiv = num1 / num2;
	return(S_OK);
}

/*Implementation of ClassFactory methods*/
CMultipllicationDivisonClassFactory::CMultipllicationDivisonClassFactory(void)
{
	m_cRef = 1;
}

CMultipllicationDivisonClassFactory:: ~CMultipllicationDivisonClassFactory(void)
{
	//code
}

HRESULT CMultipllicationDivisonClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMultipllicationDivisonClassFactory::AddRef()
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMultipllicationDivisonClassFactory::Release()
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of classFactory specific methods*/
HRESULT CMultipllicationDivisonClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CMultiplicationDivison *pCMultiplicationDivison = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);

	pCMultiplicationDivison = new CMultiplicationDivison;

	if (pCMultiplicationDivison == NULL)
		return(E_OUTOFMEMORY);
	/*get the required interface*/
	hr = pCMultiplicationDivison->QueryInterface(riid, ppv);
	pCMultiplicationDivison->Release();
	return(hr);
}

HRESULT CMultipllicationDivisonClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);
	return(S_OK);
}

/*Implementation of Exported function for this DLL*/
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	//variable declaration
	CMultipllicationDivisonClassFactory *pCMultiplicationDivisonClassFactory = NULL;
	HRESULT hr;

	if (rclsid != CLSID_MultiplicationDivison)
		return(CLASS_E_CLASSNOTAVAILABLE);
	/*create class factory*/
	pCMultiplicationDivisonClassFactory = new CMultipllicationDivisonClassFactory;
	if (pCMultiplicationDivisonClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCMultiplicationDivisonClassFactory->QueryInterface(riid, ppv);
	pCMultiplicationDivisonClassFactory->Release();
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	//code
	if ((glNumbrOfActiveComponants == 0) && (glNumberOfServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
