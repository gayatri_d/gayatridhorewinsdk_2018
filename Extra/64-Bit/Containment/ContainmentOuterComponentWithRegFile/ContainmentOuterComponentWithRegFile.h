#include<windows.h>

class ISum : public IUnknown
{
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class ISubtract : public IUnknown
{
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; //pure virtulal
};

/*CLSID of CLSID_SumSubtract*/
// {FBD800EC-9673-4409-8112-53AC63B3BBAD}
const CLSID CLSID_SumSubtract = { 0xfbd800ec, 0x9673, 0x4409,{ 0x81, 0x12, 0x53, 0xac, 0x63, 0xb3, 0xbb, 0xad } };

/*IID of ISum Interface*/
// {6253A59A-DC63-44AA-9114-23CC0E025240}
const IID IID_ISum = { 0x6253a59a, 0xdc63, 0x44aa,{ 0x91, 0x14, 0x23, 0xcc, 0xe, 0x2, 0x52, 0x40 } };

/*IID of ISubtract Interface*/
// {DED8DC98-40B7-4656-9F8A-9D0761CCD137}
const IID IID_ISubtract = { 0xded8dc98, 0x40b7, 0x4656,{ 0x9f, 0x8a, 0x9d, 0x7, 0x61, 0xcc, 0xd1, 0x37 } };
