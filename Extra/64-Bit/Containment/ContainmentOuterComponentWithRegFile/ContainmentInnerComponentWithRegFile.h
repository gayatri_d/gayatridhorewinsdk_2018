#include<windows.h>

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual 
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of MultiplicationDivison component*/
// {DAE68C16-9B96-4E19-AFCE-A7A541B993CD}
const CLSID CLSID_MultiplicationDivison = { 0xdae68c16, 0x9b96, 0x4e19,{ 0xaf, 0xce, 0xa7, 0xa5, 0x41, 0xb9, 0x93, 0xcd } };

/*IID of IMultiplication interface*/
// {7BA634A0-5F2D-45E7-87B8-6FEB15AE3B8D}
const IID IID_IMultiplication = { 0x7ba634a0, 0x5f2d, 0x45e7,{ 0x87, 0xb8, 0x6f, 0xeb, 0x15, 0xae, 0x3b, 0x8d } };

/*IID of IDivison interface*/
// {6719502E-7062-4E5F-BF25-1FD10D6B2477}
const IID IID_IDivison = { 0x6719502e, 0x7062, 0x4e5f,{ 0xbf, 0x25, 0x1f, 0xd1, 0xd, 0x6b, 0x24, 0x77 } };

