//Headers
#include"ClassFactoryDllServerWithRegFile.h"

//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
ISquare *pISquare = NULL;
ICube *pICube = NULL;

//Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ComClient");
	HRESULT hr;

	/*COM Initialization*/
	hr = CoInitialize(NULL);

	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM Library can not be initialized. \nProgram will now exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}
	//code
	//Initialization of  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register  above class
	RegisterClassEx(&wndclass);

	//Create window
	hwnd = CreateWindow(szAppName,
		TEXT("Client of COM DLL server"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//COM Uninitialition
	CoUninitialize();
	return((int)msg.wParam);
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion declaration
	void SafeInterfaceRelease(void);

	//variable declaration
	HRESULT hr = S_OK;
	int iNum, iSquare, iCube;
	TCHAR str[255];

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		hr = CoCreateInstance(CLSID_SquareCube, NULL, CLSCTX_INPROC_SERVER, IID_ISquare, (void **)&pISquare);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ISquare interface can not be loaded"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		//Initialize arguments hardcoded
		iNum = 90;
		pISquare->MakeSquare(iNum,&iSquare);
		//Display the result 
		wsprintf(str, TEXT("Square of %d is %d"), iNum, iSquare);
		MessageBox(hwnd, str, TEXT("Result"), MB_OK);
		hr = pISquare->QueryInterface(IID_ICube, (void **)&pICube);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ICube Interface can not br obtained"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		pICube->Release();
		pISquare = NULL;
		//again initialize arguments hardcoded
		iNum = 4;
		
		pICube->MakeCube(iNum, &iCube);
		pICube->Release();
		pICube = NULL;

		/*Display the result*/
		wsprintf(str, TEXT("Cube of %d is %d"), iNum, iCube);
		MessageBox(hwnd, str, TEXT("Result"), MB_OK);

		/*Exit the application*/
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void SafeInterfaceRelease(void)
{
	//
	if (pISquare)
	{
		pISquare->Release();
		pISquare = NULL;
	}
	if (pICube)
	{
		pICube->Release();
		pICube = NULL;
	}
}
