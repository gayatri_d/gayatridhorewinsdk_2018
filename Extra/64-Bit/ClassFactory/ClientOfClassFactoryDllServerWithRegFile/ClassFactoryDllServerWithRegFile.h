#include<windows.h>

class ISquare : public IUnknown
{
public:
	virtual HRESULT __stdcall MakeSquare(int, int*) = 0;
};

class ICube : public IUnknown
{
public:
	virtual HRESULT __stdcall MakeCube(int, int*) = 0;
};

/*CLSID of class implementing these two interfaces*/
// {CDFBD891-D4BE-4BB5-8161-D585E06C1582}
const CLSID CLSID_SquareCube = { 0xcdfbd891, 0xd4be, 0x4bb5,{ 0x81, 0x61, 0xd5, 0x85, 0xe0, 0x6c, 0x15, 0x82 } };

/*IID of ISquare interface*/
// {DB2F64EC-071F-4E25-9BE5-FA291BAC186D}
const IID IID_ISquare = { 0xdb2f64ec, 0x71f, 0x4e25,{ 0x9b, 0xe5, 0xfa, 0x29, 0x1b, 0xac, 0x18, 0x6d } };

/*IID of ICube interface*/
// {D4504D03-7D13-453B-8B30-0E17ED4344FA}
const IID IID_ICube = { 0xd4504d03, 0x7d13, 0x453b,{ 0x8b, 0x30, 0xe, 0x17, 0xed, 0x43, 0x44, 0xfa } };

