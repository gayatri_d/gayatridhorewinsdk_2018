#include"ClassFactoryDllServerWithRegFile.h"

/*Global varible declaration*/
long glNumberOfActiveComponents;
long glNumberOfServerLock;

/*Decelration of class implementing intefaces*/
class CSquareCube : public ISquare, ICube
{
private:
	long m_cRef;
public:
	/*Constructor method*/
	CSquareCube();
	/*Destructor Method*/
	~CSquareCube();

	/*IUnknown Specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*ISquare specific method*/
	HRESULT __stdcall MakeSquare(int, int*);

	/*ICube Specific method*/
	HRESULT __stdcall MakeCube(int, int*);
};

class CSquareCubeClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method*/
	CSquareCubeClassFactory();
	/*Desturctor Method*/
	~CSquareCubeClassFactory();
	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*IClassFactory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*Dll main*/
BOOL WINAPI DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReaserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Constructor method of CSquareCube*/
CSquareCube::CSquareCube()
{
	m_cRef = 1;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

/*Destructor method*/
CSquareCube :: ~CSquareCube()
{
	InterlockedDecrement(&glNumberOfActiveComponents);
}

/*QueryInterface*/
HRESULT CSquareCube::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<ISquare *>(this);
	else if (riid == IID_ISquare)
		*ppv = static_cast<ISquare *>(this);
	else if (riid == IID_ICube)
		*ppv = static_cast<ICube *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

/*AddRef() method*/
ULONG CSquareCube::AddRef()
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

/*Release() method*/
ULONG CSquareCube::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*ISqure method*/
HRESULT CSquareCube::MakeSquare(int num, int *result)
{
	*result = (num * num);
	return(S_OK);
}

/*ICube method*/
HRESULT CSquareCube::MakeCube(int num, int *result)
{
	*result = (num * num * num);
	return(S_OK);
}

/*CSquareCubeClassFactory constructor*/
CSquareCubeClassFactory::CSquareCubeClassFactory()
{
	m_cRef = 1;
}

CSquareCubeClassFactory ::~CSquareCubeClassFactory()
{

}

HRESULT CSquareCubeClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSquareCubeClassFactory::AddRef()
{
	InterlockedIncrement(&m_cRef);
	return(S_OK);
}

ULONG CSquareCubeClassFactory::Release()
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(S_OK);
}

/*Implementation of CSumSubtractClassFactory's IClassFactory's Method*/
HRESULT CSquareCubeClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	//variable declarations
	CSquareCube *pCSquareCube = NULL;
	HRESULT hr;

	//code
	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	//create the instance of component i.e of CSumSubtract class
	pCSquareCube = new CSquareCube;

	if (pCSquareCube == NULL)
		return(E_OUTOFMEMORY);
	//get the requested interface 
	hr = pCSquareCube->QueryInterface(riid, ppv);
	pCSquareCube->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT CSquareCubeClassFactory::LockServer(BOOL fLock)
{
	//code
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLock);
	else
		InterlockedDecrement(&glNumberOfServerLock);

	return(S_OK);
}

/*Implementation of Exported functions from this dll*/
HRESULT __stdcall DllGetClassObject(REFCLSID  rclsid, REFIID riid, void **ppv)
{
	//variable declration
	//MessageBox(NULL, TEXT("inside inner"), TEXT("Inner"), MB_OK);
	CSquareCubeClassFactory *pCSquareCubeClassFactory = NULL;
	HRESULT hr;
	//code
	if (rclsid != CLSID_SquareCube)
		return(CLASS_E_CLASSNOTAVAILABLE);
	//Create class factory
	pCSquareCubeClassFactory = new CSquareCubeClassFactory;
	if (pCSquareCubeClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCSquareCubeClassFactory->QueryInterface(riid, ppv);
	pCSquareCubeClassFactory->Release(); //anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT __stdcall DllCanUnloadnow(void)
{
	//code
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLock == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
