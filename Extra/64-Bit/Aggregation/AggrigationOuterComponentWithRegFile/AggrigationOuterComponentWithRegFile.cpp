#include"AggrigationInnerComponentWithRegFile.h"
#include"AggrigationOuterComponentWithRegFile.h"

/*global variables*/
long glNumberOfActiveComponents = 0;
long glNumberServerLocks = 0;

/*Co_class*/
class CSumSubtract : public ISum, ISubtract
{
private:
	long m_cRef;
	IUnknown *m_pIUnknownInner;
	IMultiplication *m_pIMultiplication;
	IDivison *m_pIDivison;
public:
	/*Constructor method*/
	CSumSubtract(void);
	/*Destructor method*/
	~CSumSubtract(void);
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*ISum sepcific method*/
	HRESULT __stdcall SumOfTwoIntegers(int, int, int*);
	/*ISubtract specific method*/
	HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*);
	/*Custom method for inner component creation*/
	HRESULT __stdcall InitializeInnerComponent(void);
};

/*Class Factory*/
class CSumSubtractClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method*/
	CSumSubtractClassFactory(void);
	/*Destructor method*/
	~CSumSubtractClassFactory(void);
	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*IClassFactory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*DLL main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*CSumSubtract methods implementation*/
/*Constructor method*/
CSumSubtract::CSumSubtract(void)
{
	m_cRef = 1;
	m_pIUnknownInner = NULL;
	m_pIMultiplication = NULL;
	m_pIDivison = NULL;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

/*Destructor method*/
CSumSubtract::~CSumSubtract(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents);
	if (m_pIMultiplication)
	{
		m_pIMultiplication->Release();
		m_pIMultiplication = NULL;
	}
	if (m_pIDivison)
	{
		m_pIDivison->Release();
		m_pIDivison = NULL;
	}
	if (m_pIUnknownInner)
	{
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL;
	}
}

/*Implementation of CSumSubtrat's IUnknown's Methods*/
HRESULT CSumSubtract::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISum)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISubtract)
		*ppv = static_cast<ISubtract *>(this);
	else if (riid == IID_IMultiplication)
		return(m_pIUnknownInner->QueryInterface(riid, ppv));
	else if (riid == IID_IDivison)
		return(m_pIUnknownInner->QueryInterface(riid, ppv));
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubtract::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubtract::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of ISum's method*/
HRESULT CSumSubtract::SumOfTwoIntegers(int iNum1, int iNum2, int *pSum)
{
	*pSum = iNum1 + iNum2;
	return(S_OK);
}

/*Implemmentation of ISubtract's method*/
HRESULT CSumSubtract::SubtractionOfTwoIntegers(int iNum1, int iNum2, int *pSub)
{
	*pSub = iNum1 - iNum2;
	return(S_OK);
}

/*Custom function*/
HRESULT CSumSubtract::InitializeInnerComponent(void)
{
	//variable declaration
	HRESULT hr = S_OK;

	hr = CoCreateInstance(CLSID_MultiplicationDivison,
		reinterpret_cast<IUnknown *>(this),
		CLSCTX_INPROC_SERVER,
		IID_IUnknown,
		(void **)&m_pIUnknownInner);

	/*if (m_pIUnknownInner != NULL)
	{
	MessageBox(NULL, TEXT("Inner in outer is filled"), TEXT("SUCCESS"), MB_OK);
	}*/

	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IUnknown interface can not be obtained from Inner component"), TEXT("Error"), MB_OK);
		return(E_FAIL);
	}
	hr = m_pIUnknownInner->QueryInterface(IID_IMultiplication, (void **)&m_pIMultiplication);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IMultiplication interface can not be obtained from inner component"), TEXT("Error"), MB_OK);
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL;
		return(E_FAIL);
	}
	hr = m_pIUnknownInner->QueryInterface(IID_IDivison, (void **)&m_pIDivison);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IDivison interface can not be obtained from inner component"), TEXT("Error"), MB_OK);
		m_pIUnknownInner->Release();
		m_pIUnknownInner = NULL;
		return(E_FAIL);
	}
	return(S_OK);
}

/*Implementation of ClassFactory's methods*/
/*Constructor method*/
CSumSubtractClassFactory::CSumSubtractClassFactory(void)
{
	m_cRef = 1;
}

CSumSubtractClassFactory::~CSumSubtractClassFactory(void)
{
	//code
}

HRESULT CSumSubtractClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubtractClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubtractClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of CSumSubtractClassFactory's IClassFactory's method*/
HRESULT CSumSubtractClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	/*Variable declaration*/
	CSumSubtract *pCSumSubtract = NULL;
	HRESULT hr = S_OK;

	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	/*Create the instance of component i.e CSumSubtract class*/
	pCSumSubtract = new CSumSubtract;
	if (pCSumSubtract == NULL)
		return(E_OUTOFMEMORY);
	hr = pCSumSubtract->InitializeInnerComponent();
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Failed to initialize inner component"), TEXT("Error"), MB_OK);
		pCSumSubtract->Release();
		return(hr);
	}
	/*Get the required interface*/
	hr = pCSumSubtract->QueryInterface(riid, ppv);
	pCSumSubtract->Release();
	return(hr);
}

HRESULT CSumSubtractClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberServerLocks);
	else
		InterlockedDecrement(&glNumberServerLocks);

	return(S_OK);
}

/*Implementation of Exported functions from this dll*/
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CSumSubtractClassFactory *pCSumSubtractClassFactory = NULL;
	HRESULT hr = S_OK;
	//MessageBox(NULL, TEXT("In DllGetClassObject of outer"), TEXT("Message"), MB_OK);

	if (rclsid != CLSID_SumSubtract)
		return(CLASS_E_CLASSNOTAVAILABLE);
	pCSumSubtractClassFactory = new CSumSubtractClassFactory;
	if (pCSumSubtractClassFactory == NULL)
		return(E_OUTOFMEMORY);

	hr = pCSumSubtractClassFactory->QueryInterface(riid, ppv);
	pCSumSubtractClassFactory->Release();
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) && (glNumberServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
