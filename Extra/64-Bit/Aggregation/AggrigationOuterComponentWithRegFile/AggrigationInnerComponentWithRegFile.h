#include<windows.h>

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of MultiplicationDivison component*/
// {6D674742-3F22-49B6-BCA0-8BADA047C350}
const CLSID CLSID_MultiplicationDivison = { 0x6d674742, 0x3f22, 0x49b6,{ 0xbc, 0xa0, 0x8b, 0xad, 0xa0, 0x47, 0xc3, 0x50 } };

/*IID of IMultiplication interface*/
// {CE199A43-5B57-444A-9A50-85679DC184E9}
const IID IID_IMultiplication = { 0xce199a43, 0x5b57, 0x444a,{ 0x9a, 0x50, 0x85, 0x67, 0x9d, 0xc1, 0x84, 0xe9 } };

/*IID of IDivison interface*/
// {C7CF330E-3020-4466-9B84-DFCFC7DB1ECF}
const IID IID_IDivison = { 0xc7cf330e, 0x3020, 0x4466,{ 0x9b, 0x84, 0xdf, 0xcf, 0xc7, 0xdb, 0x1e, 0xcf } };

