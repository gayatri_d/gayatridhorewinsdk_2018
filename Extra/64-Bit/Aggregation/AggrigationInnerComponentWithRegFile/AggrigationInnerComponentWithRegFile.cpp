#include"AggrigationInnerComponentWithRegFile.h"

/*Global vaiables declaration*/
long glNumberOfActiveComponents = 0;
long glNumberOfServerLocks = 0;

/*Interface declaration (for internal use only i.e not to be included in .h file)*/
interface INoAggregationIUnknown
{
	virtual HRESULT __stdcall QueryInterface_NoAggregation(REFIID, void**) = 0;
	virtual ULONG __stdcall AddRef_NoAggregation(void) = 0;
	virtual ULONG __stdcall Release_NoAggregation(void) = 0;
};

/*Class Declaration*/
class CMultiplicationDivison :public INoAggregationIUnknown, IMultiplication, IDivison
{
private:
	long m_cRef;
	IUnknown *m_pIUnknownOuter;
public:
	/*Constructor method declaration*/
	CMultiplicationDivison(IUnknown *);
	/*Destructor method*/
	~CMultiplicationDivison(void);
	/*Aggregation supported IUnknown specific method  declaration (inherited)*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*Aggregation not supported IUnknown specific methods declaration (inherited)*/
	HRESULT __stdcall QueryInterface_NoAggregation(REFIID, void**);
	ULONG __stdcall AddRef_NoAggregation(void);
	ULONG __stdcall Release_NoAggregation(void);
	/*IMultiplication specific method declaration(inherited)*/
	HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*);
	/*IDivison specific method declaration(inherited)*/
	HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*);
};

/*Class Factory*/
class CMultiplicationDivisonClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*constructor method declaration*/
	CMultiplicationDivisonClassFactory(void);
	/*Destructor method declaration*/
	~CMultiplicationDivisonClassFactory(void);
	/*IUnknown specific method declarations*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);
	/*IClassFactory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*DLL main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reaserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Implemetation of CMultiplicationDivison's methods*/
/*Constructor method*/
CMultiplicationDivison::CMultiplicationDivison(IUnknown *pIUnknownOuter)
{
	m_cRef = 1;
	InterlockedIncrement(&glNumberOfActiveComponents);

	if (pIUnknownOuter != NULL)
	{
		m_pIUnknownOuter = pIUnknownOuter;
	}
	else
		m_pIUnknownOuter = reinterpret_cast<IUnknown *>(static_cast<INoAggregationIUnknown *>(this));
}

/*Destructor method*/
CMultiplicationDivison::~CMultiplicationDivison(void)
{
	//code
	InterlockedDecrement(&glNumberOfActiveComponents);
}

/*Implementation of CMultiplicationDivison's Aggrigation supporting IUnknown's methods*/
HRESULT CMultiplicationDivison::QueryInterface(REFIID riid, void** ppv)
{
	//code
	return(m_pIUnknownOuter->QueryInterface(riid, ppv));
}

ULONG CMultiplicationDivison::AddRef(void)
{
	return(m_pIUnknownOuter->AddRef());
}

ULONG CMultiplicationDivison::Release(void)
{
	return(m_pIUnknownOuter->Release());
}

/*Implementation of CMultiplicationDivison's Aggregation NonSupporting IUnknown's methods*/
HRESULT CMultiplicationDivison::QueryInterface_NoAggregation(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<INoAggregationIUnknown *>(this);
	else if (riid == IID_IMultiplication)
		*ppv = static_cast<IMultiplication *>(this);
	else if (riid == IID_IDivison)
		*ppv = static_cast<IDivison *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMultiplicationDivison::AddRef_NoAggregation(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMultiplicationDivison::Release_NoAggregation(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implemetation of IMultiplication's method*/
HRESULT CMultiplicationDivison::MultiplicationOfTwoIntegers(int iNum1, int iNum2, int *pMul)
{
	*pMul = iNum1 * iNum2;
	return(S_OK);
}

/*Implementation of IDivison's method*/
HRESULT CMultiplicationDivison::DivisonOfTwoIntegers(int iNum1, int iNum2, int *pDiv)
{
	*pDiv = iNum1 / iNum2;
	return(S_OK);
}

/*Implementation of CMultiplicationDivisonClassFactory's methods*/
CMultiplicationDivisonClassFactory::CMultiplicationDivisonClassFactory(void)
{
	m_cRef = 1;
}

CMultiplicationDivisonClassFactory::~CMultiplicationDivisonClassFactory(void)
{
	//code
}

/*Implementation of CMultiplicationDivisonClassFactory's IClassFactory's IUnknown's Methods*/
HRESULT CMultiplicationDivisonClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMultiplicationDivisonClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMultiplicationDivisonClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of CMultiplicationDivisonClassFactory's IClassFactory's Methods*/
HRESULT CMultiplicationDivisonClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	/*variable declaration*/
	CMultiplicationDivison *pCMultiplicationDivison = NULL;
	HRESULT hr = S_OK;

	if ((pUnkOuter != NULL) && (riid != IID_IUnknown))
		return(CLASS_E_NOAGGREGATION);

	pCMultiplicationDivison = new CMultiplicationDivison(pUnkOuter);
	if (pCMultiplicationDivison == NULL)
		return(E_OUTOFMEMORY);
	hr = pCMultiplicationDivison->QueryInterface_NoAggregation(riid, ppv);
	pCMultiplicationDivison->Release_NoAggregation();
	return(hr);
}

HRESULT CMultiplicationDivisonClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);
	return(S_OK);
}

/*Implementation of Exportable functions*/
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CMultiplicationDivisonClassFactory *pCMultiplicationDivisonClassFactory = NULL;
	HRESULT hr = S_OK;

	if (rclsid != CLSID_MultiplicationDivison)
		return(CLASS_E_CLASSNOTAVAILABLE);

	pCMultiplicationDivisonClassFactory = new CMultiplicationDivisonClassFactory;

	if (pCMultiplicationDivisonClassFactory == NULL)
		return(E_OUTOFMEMORY);

	hr = pCMultiplicationDivisonClassFactory->QueryInterface(riid, ppv);
	pCMultiplicationDivisonClassFactory->Release();
	return(hr);
}

HRESULT  __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
