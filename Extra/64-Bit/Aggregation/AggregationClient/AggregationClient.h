#include<windows.h>

class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;
};

class ISubtract : public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};


/*CLSID of SumSubtract component */
// {64FC1022-9FFF-42C0-AFB1-FF6F1871E26C}
const CLSID CLSID_SumSubtract = { 0x64fc1022, 0x9fff, 0x42c0,{ 0xaf, 0xb1, 0xff, 0x6f, 0x18, 0x71, 0xe2, 0x6c } };

/*IID of ISum interface */
// {6752A639-0E45-4484-97B6-B6CD08585A32}
const IID IID_ISum = { 0x6752a639, 0xe45, 0x4484,{ 0x97, 0xb6, 0xb6, 0xcd, 0x8, 0x58, 0x5a, 0x32 } };

/*IID of  ISubtract interface */
// {C23B0C33-467E-44B8-94B4-6FFE7774DDE9}
const IID IID_ISubtract = { 0xc23b0c33, 0x467e, 0x44b8,{ 0x94, 0xb4, 0x6f, 0xfe, 0x77, 0x74, 0xdd, 0xe9 } };

/*IID of IMultiplication interface*/
// {CE199A43-5B57-444A-9A50-85679DC184E9}
const IID IID_IMultiplication = { 0xce199a43, 0x5b57, 0x444a,{ 0x9a, 0x50, 0x85, 0x67, 0x9d, 0xc1, 0x84, 0xe9 } };

/*IID of IDivison interface*/
// {C7CF330E-3020-4466-9B84-DFCFC7DB1ECF}
const IID IID_IDivison = { 0xc7cf330e, 0x3020, 0x4466,{ 0x9b, 0x84, 0xdf, 0xcf, 0xc7, 0xdb, 0x1e, 0xcf } };


