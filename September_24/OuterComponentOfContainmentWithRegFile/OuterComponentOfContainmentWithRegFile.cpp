#include"InnerComponentOfContainmentWithRegFile.h"
#include"OuterComponentOfContainmentWithRegFile.h"

/*Global variables*/
long glNumberOfActiveComponents = 0;
long glNumberOfServerLocks = 0;

/*Co-class*/
class CSumSubtract : public ISum, ISubtract, IMultiplication, IDivison
{
private:
	long m_cRef;
	IMultiplication *m_pIMultiplication;
	IDivison *m_pIDivison;
public:
	/*Constructor method declration*/
	CSumSubtract(void);
	/*Destructor method implementation*/
	~CSumSubtract(void);
	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void **);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*ISum Specific method decaration*/
	HRESULT __stdcall SumOfTwoIntegers(int, int, int*);

	/*ISubtract specific method declaration*/
	HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*);

	/*IMultiplication specific method*/
	HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*);

	/*IDivison specific method*/
	HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*);

	/*Custom Method for inner compnent creation*/
	HRESULT __stdcall InitializeInnerComponent(void);
};

/*Class Factory */
class CSumSubtractClassFactory : public IClassFactory
{
private:
	long m_cRef;
public:
	/*Constructor method declration*/
	CSumSubtractClassFactory(void);
	/*Destructor method declaration*/
	~CSumSubtractClassFactory(void);
	/*IUnknown specific methods*/
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	/*Class Factory specific methods*/
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

/*DLL Main*/
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;

	case DLL_THREAD_ATTACH:
		break;

	case DLL_THREAD_DETACH:
		break;

	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

/*Co-Class methods implementation*/

CSumSubtract::CSumSubtract(void)
{
	m_cRef = 1;
	m_pIMultiplication = NULL;
	m_pIDivison = NULL;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

CSumSubtract::~CSumSubtract(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents);
	if (m_pIMultiplication)
	{
		m_pIMultiplication->Release();
		m_pIMultiplication = NULL;
	}
	if (m_pIDivison)
	{
		m_pIDivison->Release();
		m_pIDivison = NULL;
	}
}

/*Implementation of IUnknown spcific methods*/
HRESULT CSumSubtract::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISum)
		*ppv = static_cast<ISum *>(this);
	else if (riid == IID_ISubtract)
		*ppv = static_cast<ISubtract *>(this);
	else if (riid == IID_IMultiplication)
		*ppv = static_cast<IMultiplication *>(this);
	else if (riid == IID_IDivison)
		*ppv = static_cast<IDivison *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubtract::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubtract::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of ISum specific method*/
HRESULT CSumSubtract::SumOfTwoIntegers(int num1, int num2, int *pSum)
{
	*pSum = num1 + num2;
	return(S_OK);
}

/*Implementation of ISubtract specific method*/
HRESULT CSumSubtract::SubtractionOfTwoIntegers(int num1, int num2, int *pSub)
{
	*pSub = num1 - num2;
	return(S_OK);
}

/*Implemetation of IMultiplication's method*/
HRESULT CSumSubtract::MultiplicationOfTwoIntegers(int num1, int num2, int *pMul)
{
	m_pIMultiplication->MultiplicationOfTwoIntegers(num1, num2, pMul);
	return(S_OK);
}

/*Implementation of IDivison's method*/
HRESULT CSumSubtract::DivisonOfTwoIntegers(int num1, int num2, int *pDiv)
{
	m_pIDivison->DivisonOfTwoIntegers(num1, num2, pDiv);
	return(S_OK);
}

HRESULT CSumSubtract::InitializeInnerComponent(void)
{
	HRESULT hr;
	hr = CoCreateInstance(CLSID_MultiplicationDivison, NULL, CLSCTX_INPROC_SERVER, IID_IMultiplication, (void**)&m_pIMultiplication);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IMultiplication Interface can not be Obtained from inner component"), TEXT("Error"), MB_OK);
		return(E_FAIL);
	}
	hr = m_pIMultiplication->QueryInterface(IID_IDivison, (void **)&m_pIDivison);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("IDivison Interface can not be obtained from inner component"), TEXT("Error"), MB_OK);
		return(E_FAIL);
	}
	return(S_OK);
}

/*Implementation of CSumSubtractClassFactory's methods*/
CSumSubtractClassFactory::CSumSubtractClassFactory(void)
{
	m_cRef = 1;
}

CSumSubtractClassFactory::~CSumSubtractClassFactory(void)
{
	//code
}

HRESULT CSumSubtractClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}
	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CSumSubtractClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CSumSubtractClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

/*Implementation of CSumSubtractClassFactory's Methods*/
HRESULT CSumSubtractClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	CSumSubtract *pCSumSubtract = NULL;
	HRESULT hr;
	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);
	pCSumSubtract = new CSumSubtract;
	if (pCSumSubtract == NULL)
		return(E_OUTOFMEMORY);

	hr = pCSumSubtract->InitializeInnerComponent();
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Failed to initialize inner componanet"), TEXT("Error"), MB_OK);
		pCSumSubtract->Release();
		return(hr);
	}
	/*Get requested interface*/
	hr = pCSumSubtract->QueryInterface(riid, ppv);
	pCSumSubtract->Release();
	return(hr);
}

HRESULT CSumSubtractClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);
	return(S_OK);
}

/*Implementation of Exportable functions*/
HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void **ppv)
{
	CSumSubtractClassFactory *pCSumSubtractClassFactory = NULL;
	HRESULT hr;
	if (rclsid != CLSID_SumSubtract)
		return(CLASS_E_CLASSNOTAVAILABLE);
	pCSumSubtractClassFactory = new CSumSubtractClassFactory;
	if (pCSumSubtractClassFactory == NULL)
		return(E_OUTOFMEMORY);
	hr = pCSumSubtractClassFactory->QueryInterface(riid, ppv);
	pCSumSubtractClassFactory->Release();
	return(hr);
}

HRESULT __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLocks == 0))
		return(S_OK);
	else
		return(S_FALSE);
}
