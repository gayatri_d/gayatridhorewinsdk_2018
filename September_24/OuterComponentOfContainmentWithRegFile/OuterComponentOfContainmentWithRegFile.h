#include<windows.h>

class ISum : public IUnknown
{
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class ISubtract : public IUnknown
{
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; //pure virtulal
};

/*CLSID of CLSID_SumSubtract*/
// {858A2FD8-3B08-4D05-ABFD-715771DB50DE}
const CLSID CLSID_SumSubtract = { 0x858a2fd8, 0x3b08, 0x4d05, 0xab, 0xfd, 0x71, 0x57, 0x71, 0xdb, 0x50, 0xde };

/*IID of ISum Interface*/
// {B70C758C-6761-48AF-ADF9-F517087DE6A8}
const IID IID_ISum = { 0xb70c758c, 0x6761, 0x48af, 0xad, 0xf9, 0xf5, 0x17, 0x8, 0x7d, 0xe6, 0xa8 };

/*IID of ISubtract Interface*/
// {D65D07B3-0375-480F-81F4-35DE74C2F164}
const IID IID_ISubtract = { 0xd65d07b3, 0x375, 0x480f, 0x81, 0xf4, 0x35, 0xde, 0x74, 0xc2, 0xf1, 0x64 };
