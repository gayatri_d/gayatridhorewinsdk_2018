/*Include header file given by server*/
#include"ClientOfContainmentComponanetWithRegFile.h"

//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//global variable declaration
ISum *pISum = NULL;
ISubtract *pISubtract = NULL;
IMultiplication *pIMultiplication;
IDivison *pIDivison;

//Winmain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("ContaimentClient");
	HRESULT hr;

	/*COM Initialization*/
	hr = CoInitialize(NULL);

	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM Library can not be initialized. \nProgram will now exit."), TEXT("Program Error"), MB_OK);
		exit(0);
	}
	//code
	//Initialization of  WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//register  above class
	RegisterClassEx(&wndclass);

	//Create window
	hwnd = CreateWindow(szAppName,
		TEXT("Client of COM Containment DLL server"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//COM Uninitialition
	CoUninitialize();
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion declaration
	void SafeInterfaceRelease(void);

	//variable declaration
	HRESULT hr = S_OK;
	int iNum1, iNum2, iSum, iSub, iMultiply, iDiv;
	TCHAR str[255];

	//code
	switch (iMsg)
	{
	case WM_CREATE:
		hr = CoCreateInstance(CLSID_SumSubtract, NULL, CLSCTX_INPROC_SERVER, IID_ISum, (void **)&pISum);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ISum interface can not be loaded"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		//Initialize arguments hardcoded
		iNum1 = 90;
		iNum2 = 12;

		//call SumOfTwoIntegers() of ISum to get sum
		pISum->SumOfTwoIntegers(iNum1, iNum2, &iSum);
		//Display the result 
		wsprintf(str, TEXT("Sum of %d and %d = %d"), iNum1, iNum2, iSum);
		MessageBox(hwnd, str, TEXT("Result"), MB_OK);
		//call QueryInterface() on ISum to get ISubtract's pointer
		hr = pISum->QueryInterface(IID_ISubtract, (void **)&pISubtract);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("ISubtract Interface can not br obtained"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		pISum->Release();
		pISum = NULL;
		//again initialize arguments hardcoded
		iNum1 = 245;
		iNum2 = 100;
		//Call SubtractionOfTwoIntegers() of ISubtract to get subtraction
		pISubtract->SubtractionOfTwoIntegers(iNum1, iNum2, &iSub);
		/*Display the result*/
		wsprintf(str, TEXT("Subtraction of %d and %d = %d"), iNum1, iNum2, iSub);
		MessageBox(hwnd, str, TEXT("Result"), MB_OK);
		/*Get Multipllication Interface*/
		hr = pISubtract->QueryInterface(IID_IMultiplication, (void**)&pIMultiplication);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IMultiplication interface can  not be obtained"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		//as ISubtract is not needed now onwords, release it
		pISubtract->Release();
		pISubtract = NULL;
		iNum1 = 18;
		iNum2 = 34;
		pIMultiplication->MultiplicationOfTwoIntegers(iNum1, iNum2, &iMultiply);
		/*Display the result*/
		wsprintf(str, TEXT("Multiplication of %d and %d = %d"), iNum1, iNum2, iMultiply);
		MessageBox(hwnd, str, TEXT("Result"), MB_OK);
		/*Get interface for Divison*/
		hr = pIMultiplication->QueryInterface(IID_IDivison, (void**)&pIDivison);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IDivison interface can not be obtained"), TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		/*as IMultiplication is not neede now onwords release it*/
		pIMultiplication->Release();
		pIMultiplication = NULL;

		/*hardcoded function arguments*/
		iNum1 = 50;
		iNum2 = 5;

		pIDivison->DivisonOfTwoIntegers(iNum1, iNum2, &iDiv);

		/*Display the reasult*/
		wsprintf(str, TEXT("Divison of %d and %d = %d"), iNum1, iNum2, iDiv);
		MessageBox(hwnd, str, TEXT("Error"), MB_OK);

		/*As IDivison is not needed now onwords release it*/
		pIDivison->Release();
		pIDivison = NULL;

		/*Exit the application*/
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void SafeInterfaceRelease(void)
{
	//code
	if (pISum)
	{
		pISum->Release();
		pISum = NULL;
	}
	if (pISubtract)
	{
		pISubtract->Release();
		pISubtract = NULL;
	}
	if (pIMultiplication)
	{
		pIMultiplication->Release();
		pIMultiplication = NULL;
	}
	if (pIDivison)
	{
		pIDivison->Release();
		pIDivison = NULL;
	}
}
