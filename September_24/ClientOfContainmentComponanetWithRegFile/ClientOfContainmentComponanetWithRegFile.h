#include<windows.h>

class ISum : public IUnknown
{
public:
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

class ISubtract : public IUnknown
{
public:
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; //pure virtulal
};

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual 
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of CLSID_SumSubtract*/
// {858A2FD8-3B08-4D05-ABFD-715771DB50DE}
const CLSID CLSID_SumSubtract = { 0x858a2fd8, 0x3b08, 0x4d05, 0xab, 0xfd, 0x71, 0x57, 0x71, 0xdb, 0x50, 0xde };

/*IID of ISum Interface*/
// {B70C758C-6761-48AF-ADF9-F517087DE6A8}
const IID IID_ISum = { 0xb70c758c, 0x6761, 0x48af, 0xad, 0xf9, 0xf5, 0x17, 0x8, 0x7d, 0xe6, 0xa8 };

/*IID of ISubtract Interface*/
// {D65D07B3-0375-480F-81F4-35DE74C2F164}
const IID IID_ISubtract = { 0xd65d07b3, 0x375, 0x480f, 0x81, 0xf4, 0x35, 0xde, 0x74, 0xc2, 0xf1, 0x64 };

/*IID of IMultiplication interface*/
// {52BB4E79-C74F-4330-80B6-24ECC0562695}
const IID IID_IMultiplication = { 0x52bb4e79, 0xc74f, 0x4330, 0x80, 0xb6, 0x24, 0xec, 0xc0, 0x56, 0x26, 0x95 };

/*IID of IDivison interface*/
// {8316BBFF-A8A5-4FC4-AA47-2215DEE37834}
const IID IID_IDivison = { 0x8316bbff, 0xa8a5, 0x4fc4, 0xaa, 0x47, 0x22, 0x15, 0xde, 0xe3, 0x78, 0x34 };
