#include<windows.h>

class IMultiplication : public IUnknown
{
public:
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0; //pure virtual 
};

class IDivison : public IUnknown
{
public:
	virtual HRESULT __stdcall DivisonOfTwoIntegers(int, int, int*) = 0; //pure virtual
};

/*CLSID of MultiplicationDivison component*/
// {F0731372-5F70-464E-96D6-C3FBF7F38ABF}
const CLSID CLSID_MultiplicationDivison = { 0xf0731372, 0x5f70, 0x464e, 0x96, 0xd6, 0xc3, 0xfb, 0xf7, 0xf3, 0x8a, 0xbf };

/*IID of IMultiplication interface*/
// {52BB4E79-C74F-4330-80B6-24ECC0562695}
const IID IID_IMultiplication = { 0x52bb4e79, 0xc74f, 0x4330, 0x80, 0xb6, 0x24, 0xec, 0xc0, 0x56, 0x26, 0x95 };

/*IID of IDivison interface*/
// {8316BBFF-A8A5-4FC4-AA47-2215DEE37834}
const IID IID_IDivison = { 0x8316bbff, 0xa8a5, 0x4fc4, 0xaa, 0x47, 0x22, 0x15, 0xde, 0xe3, 0x78, 0x34 };
